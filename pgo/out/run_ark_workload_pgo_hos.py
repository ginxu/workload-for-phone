# 检测平台
# 推文件
# 改权限
# 执行文件
# 处理结果
import datetime
import inspect
import os
import re
import subprocess
import logging
import sys
import pandas as pd
from collections import defaultdict

WORK_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # 当前脚本工作目录
# fooo:9234ms
# REGX代表这次处理要识别的正则，注意里边有两个括号部分，是要提取的项；优先级从上往下试探匹配，遇到成功则进行匹配返回，不再继续向下。
REGEX = [
    r'\s*(.*\w+.*).*:\s*(\d+\.\d+).+ms',  # Function Call - RunNormalCall:  44.46125        ms
    r'\s*(.*\w+.*).*:(\d+)ms',  # Array Access - RunFastaRandom2: 30 ms
    r'\s*(.*\w+.*).*:\s*(\d+).+ms',  # 'Splay:  3224    ms'
    r'^(\w+): ms = (\d+.\d+)',  # 'box2d: ms = 270.03125'
    r'^(\S+): ms = (\d+.\d+)',  # 'crypto-sha1: ms = 270.03125'
    r'^(\S+): ms = (\d+)',  # 'crypto-aes: ms = 40'
    r'^(\S+): ms =\s+(\d+.\d+)',  # 'crypto-aes:  ms = 40.1213'
    r'^(\S+): ms =\s+(\d+)',  # 'crypto-aes: ms  = 40'
]
ITERATION_TIME = 3  # 重复几次取平均值
rc = [re.compile(v) for v in REGEX]
TAG = 'PGO_HOS'
# rg = re.search( r'\s*(.*\S+.*)\s*:\s+(\d+).+ms', s)

DEV_DATA_LOCAL_TMP = '/data/local/tmp'
WORKLOAD = ''
TOOLS_PART = 'tools'
HOS_LIB = 'hos'
CSV = ''
PGO = True


def linux_path_join(*kwargs):
    """指定生成linux风格的拼接路径，即/分割"""
    return os.path.join(*kwargs).replace('\\', '/')


DEV_HOS_LIB_PATH = linux_path_join(DEV_DATA_LOCAL_TMP, HOS_LIB)
ARK_JS_VM = linux_path_join(DEV_HOS_LIB_PATH, 'ark_js_vm')
ADB = 'adb'


def init(work):
    """初始化"""
    RESULT_FILE_NAME_PREFIX = f"{'' if PGO else 'NO'}{TAG}_{datetime.datetime.now().strftime('%Y%m%d_%H%M%S')}"
    file = os.path.join(WORK_DIR, f"{RESULT_FILE_NAME_PREFIX}_{work}.log")
    logging.basicConfig(filename=file, level=logging.DEBUG, format='%(asctime)s %(message)s')
    return os.path.join(WORK_DIR, f"{RESULT_FILE_NAME_PREFIX}_{work}.csv")


def reg_get(s):
    """按优先级试探这行能否匹配出对应的数据"""
    for v in rc:
        res = v.search(s)
        if res:
            return res.group(1), res.group(2)
    return None, None


def exe_cmd_output(cmd, log_output=True):
    """执行构造好的编译命令"""
    try:
        logging.info(cmd)
        out = subprocess.check_output(cmd, shell=False)
        s = out.decode()
        if log_output:
            logging.info(s)
        return s
    except Exception as e:
        logging.error(e)


def get_abcs(path):
    """获取所有可执行文件"""
    if not os.path.exists(path):
        return []
    res = []
    for (root, dirs, files) in os.walk(os.path.join(WORK_DIR, WORKLOAD)):
        if root == path:
            for file in files:
                if file.endswith('.abc'):
                    res.append(file[:-4])
    return res


def push_exe(name):
    """把case都推上去，并添加可执行权限"""
    exe_cmd_output([ADB, 'shell', 'rm', '-rf', linux_path_join(DEV_DATA_LOCAL_TMP, name)])
    print([ADB, 'push', name, DEV_DATA_LOCAL_TMP])
    exe_cmd_output([ADB, 'push', name, DEV_DATA_LOCAL_TMP], False)


# print(linux_path_join(DEV_DATA_LOCAL_TMP,'a','b'))

def push_lib():
    """把需要的lib推上去"""
    exe_cmd_output([ADB, 'shell', 'rm', '-rf', DEV_HOS_LIB_PATH])
    print([ADB, 'push', os.path.join(WORK_DIR, '..', TOOLS_PART, HOS_LIB), DEV_DATA_LOCAL_TMP])
    exe_cmd_output([ADB, 'push', os.path.join(WORK_DIR, '..', TOOLS_PART, HOS_LIB), DEV_DATA_LOCAL_TMP], False)
    exe_cmd_output([ADB, 'shell', 'chmod', '+x', linux_path_join(DEV_HOS_LIB_PATH, '*')])


def exe_one(case):
    """在设备执行一个case，且尝试提取case_name+时间"""
    # s = exe_cmd_output(
    #     [ADB, 'shell', 'export', f'LD_LIBRARY_PATH={DEV_HOS_LIB_PATH}', '&&', ARK_JS_VM, f'--entry-point={os.path.basename(case)}',
    #      f'--aot-file={os.path.basename(case)}', f'{case}.abc'])
    # cd进那个目录跑
    path = os.path.dirname(case).replace('\\', '/')
    name = os.path.basename(case)
    print(path, name)
    cmd = ([ADB, 'shell', 'export', f'LD_LIBRARY_PATH={DEV_HOS_LIB_PATH}', '&&', 'cd', path, '&&', ARK_JS_VM,
            f'--entry-point={name}', '--compiler-enable-lowering-builtin=true']
           + ([f'--aot-file={name}'] if PGO else [])  # 看看跑PGO还是解释器
           + [f'--icu-data-path={DEV_HOS_LIB_PATH}']
           + [f'{name}.abc'])
    s = exe_cmd_output(cmd)
    # print(s)
    res = []
    if s:
        for line in s.split('\n'):
            case_name, t = reg_get(line)
            if case_name:
                res.append((case_name, t))
                print(case_name, t)
    return res


def exe_cases(cases, times=ITERATION_TIME):
    """在设备里执行所有case,并且执行times次，返回所有结果"""
    cnt = defaultdict(list)
    for case in cases:
        for _ in range(times):
            for case_name, t in exe_one(linux_path_join(DEV_DATA_LOCAL_TMP, WORKLOAD, case)):
                cnt[case_name].append(t)
    return cnt


def to_csv(cnt, csv_name):
    """把结果计算取平均并存csv"""
    mx_l = max(len(v) for v in cnt.values())  # 有的case可能跑失败一组，补None到等长
    for v in cnt.values():
        if len(v) < mx_l:
            v.extend([None] * (mx_l - len(v)))
    df = pd.DataFrame(cnt).T
    print(df)
    df = df.astype(float)
    df['avg'] = df.apply(lambda row: row.dropna().values.mean(), axis=1)  # 计算平均数需要去掉NaN
    print(df)
    c = df.columns.tolist()
    df = df[c[-1:] + c[:-1]]
    df.to_csv(csv_name, index=True, index_label='case_name')


def print_help():
    print("""usage:    python run_ark_workload_pgo_hos.py DIR_NAME(文件夹名字) ITERATION(重复几次取平均) [no_pgo/pgo](可选)
    example:
        python run_ark_workload_pgo_hos.py test1 5 [no_pgo]
    注意地桑耳参数如果省略，则脚本将默认跑测带pgo的数据；若指定，则跑测不带pgo的数据
    """)


def main():
    if not sys.platform.startswith('win'):
        print('本程序需在win上运行')
        return
    if len(sys.argv) not in {3, 4}:  # 严格检查参数
        return print_help()

    global WORKLOAD, PGO
    if len(sys.argv) == 4:
        pgo_tag = sys.argv[3].lower()
        if pgo_tag not in {'pgo', 'no_pgo'}:
            print('参数3只能是 [pgo] 或 [no_pgo]')
            return print_help()
        if pgo_tag == 'no_pgo':
            PGO = False

    WORKLOAD = sys.argv[1]
    cases = get_abcs(os.path.join(WORK_DIR, WORKLOAD))
    if not cases:
        print(f'指定的目录{WORKLOAD}不存在，或目录内容无.abc可执行文件')
        return
    global ITERATION_TIME
    ITERATION_TIME = int(sys.argv[2])
    print(f'---------------------------------------------------------------------------------')
    print(f'-------------------[INFO]当前运行在{"PGO" if PGO else "NO_PGO"}模式下-----------------------')
    print(f'---------------------------------------------------------------------------------')
    root_from = r'root\update_pkg_res\BINs\get_root.bat'
    y = input(f'请确认你的设备已经操作过root({root_from})[Y/N]')
    if y.upper() != 'Y':
        return

    start_time = datetime.datetime.now()
    csv = init(WORKLOAD)
    push_exe(WORKLOAD)  # 推文件到手机，并赋权
    push_lib()
    cnt = exe_cases(cases, ITERATION_TIME)  # 每个case执行多次，取平均
    to_csv(cnt, csv)  # 出表
    print("测试脚本共计花费时间[", (datetime.datetime.now() - start_time).seconds, ']秒')


if __name__ == '__main__':
    main()
