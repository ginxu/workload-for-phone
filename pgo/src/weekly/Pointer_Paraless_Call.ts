// import { BenchmarkRunner } from "../../../utils/benchmarkTsSuite";
// declare function print(arg:string) : string;

declare interface ArkTools{
  timeInUs(arg:any):number
}

class Obj {
  value: number = 0
  constructor(value: number) {
    this.value = value
  }
};

function GenerateFakeRandomObject(): Obj[] {
  let resource: Obj[] = new Array(15).fill(new Obj(0));
  for (let i = 0; i < 15; i++) {
    let random = Math.random() * (10) + 1;
    resource[i] = new Obj(random)
  }
  return resource;
}

let global_value = 0;

function GenerateFakeRandomInteger(): Int32Array {
  let resource: Int32Array = new Int32Array([12, 43, 56, 76, 89, 54, 45, 32, 35, 47, 46, 44, 21, 37, 84]);
  return resource;
}

let arr: Int32Array = GenerateFakeRandomInteger()
/***** Without parameters *****/

/***************** No parameters *****************/
function ParameterlessFoo() : Obj {
  let res: Obj[] = [new Obj(0), new Obj(0), new Obj(0), new Obj(0), new Obj(0)];
  let resources: Obj[] = GenerateFakeRandomObject();
  for (let i = 0; i < 200; i++) {
    res[i % 5] = resources[i % 15];
  }
  return res[1];
}
function CallParameterlessFoo(f:() => Obj) : Obj {
  return f();
}
export function RunParameterlessFunctionPtr():number {
  let count : number = 10000;
  global_value = 0;
  let i3 : Obj = new Obj(1);
  let startTime = ArkTools.timeInUs();
  for(let i=0;i<count;i++){
    i3 = CallParameterlessFoo(ParameterlessFoo);
  }
  let midTime = ArkTools.timeInUs();
  for(let i=0;i<count;i++) {
  }
  let endTime = ArkTools.timeInUs();
  let time = ((midTime - startTime) - (endTime - midTime)) / 1000
  print("Function Call - RunParameterlessFunctionPtr:\t"+String(time)+"\tms");
  return time
}
RunParameterlessFunctionPtr()

print("Ts Method Call Is End, global_value value: \t" + String(global_value));

