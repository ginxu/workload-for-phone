/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const EARLEYBOYER_2: number = 2;
const EARLEYBOYER_3: number = 3;
const EARLEYBOYER_4: number = 4;
const EARLEYBOYER_5: number = 5;
const EARLEYBOYER_6: number = 6;
const EARLEYBOYER_7: number = 7;
const EARLEYBOYER_120: number = 120;
const EARLEYBOYER_132: number = 132;
const EARLEYBOYER_1000: number = 1000;
const EARLEYBOYER_95024: number = 95024;

class ScPair {
  car: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  cdr: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  constructor(
    car: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
    cdr: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
  ) {
    this.car = car;
    this.cdr = cdr;
  }
  
}

class ScNull {
  constructor() {}
}

let scConst4Nboyer = new ScPair(
  '\u1E9Cimplies',
  new ScPair(
    new ScPair(
      '\u1E9Cand',
      new ScPair(
        new ScPair('\u1E9Cimplies', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
        new ScPair(
          new ScPair(
            '\u1E9Cand',
            new ScPair(
              new ScPair('\u1E9Cimplies', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))),
              new ScPair(
                new ScPair(
                  '\u1E9Cand',
                  new ScPair(
                    new ScPair('\u1E9Cimplies', new ScPair('\u1E9Cz', new ScPair('\u1E9Cu', new ScNull()))),
                    new ScPair(new ScPair('\u1E9Cimplies', new ScPair('\u1E9Cu', new ScPair('\u1E9Cw', new ScNull()))), new ScNull())
                  )
                ),
                new ScNull()
              )
            )
          ),
          new ScNull()
        )
      )
    ),
    new ScPair(new ScPair('\u1E9Cimplies', new ScPair('\u1E9Cx', new ScPair('\u1E9Cw', new ScNull()))), new ScNull())
  )
);
let scConst3Nboyer = scList(
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Ccompile', new ScPair('\u1E9Cform', new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Creverse',
          new ScPair(
            new ScPair(
              '\u1E9Ccodegen',
              new ScPair(
                new ScPair('\u1E9Coptimize', new ScPair('\u1E9Cform', new ScNull())),
                new ScPair(new ScPair('\u1E9Cnil', new ScNull()), new ScNull())
              )
            ),
            new ScNull()
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Ceqp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Cequal',
          new ScPair(
            new ScPair('\u1E9Cfix', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair(new ScPair('\u1E9Cfix', new ScPair('\u1E9Cy', new ScNull())), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cgreaterp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cy', new ScPair('\u1E9Cx', new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Clesseqp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(
        new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cy', new ScPair('\u1E9Cx', new ScNull()))), new ScNull())),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cgreatereqp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(
        new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cboolean', new ScPair('\u1E9Cx', new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cor',
          new ScPair(
            new ScPair('\u1E9Cequal', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Ct', new ScNull()), new ScNull()))),
            new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cf', new ScNull()), new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Ciff', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Cimplies', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
            new ScPair(new ScPair('\u1E9Cimplies', new ScPair('\u1E9Cy', new ScPair('\u1E9Cx', new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Ceven1', new ScPair('\u1E9Cx', new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Czerop', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair(
              new ScPair('\u1E9Ct', new ScNull()),
              new ScPair(new ScPair('\u1E9Codd', new ScPair(new ScPair('\u1E9Csub1', new ScPair('\u1E9Cx', new ScNull())), new ScNull())), new ScNull())
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Ccountps-', new ScPair('\u1E9Cl', new ScPair('\u1E9Cpred', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Ccountps-loop',
          new ScPair('\u1E9Cl', new ScPair('\u1E9Cpred', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cfact-', new ScPair('\u1E9Ci', new ScNull())),
      new ScPair(new ScPair('\u1E9Cfact-loop', new ScPair('\u1E9Ci', new ScPair(1, new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Creverse-', new ScPair('\u1E9Cx', new ScNull())),
      new ScPair(new ScPair('\u1E9Creverse-loop', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cnil', new ScNull()), new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cdivides', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(
        new ScPair('\u1E9Czerop', new ScPair(new ScPair('\u1E9Cremainder', new ScPair('\u1E9Cy', new ScPair('\u1E9Cx', new ScNull()))), new ScNull())),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cassume-true', new ScPair('\u1E9Cvar', new ScPair('\u1E9Calist', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Ccons',
          new ScPair(
            new ScPair('\u1E9Ccons', new ScPair('\u1E9Cvar', new ScPair(new ScPair('\u1E9Ct', new ScNull()), new ScNull()))),
            new ScPair('\u1E9Calist', new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cassume-false', new ScPair('\u1E9Cvar', new ScPair('\u1E9Calist', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Ccons',
          new ScPair(
            new ScPair('\u1E9Ccons', new ScPair('\u1E9Cvar', new ScPair(new ScPair('\u1E9Cf', new ScNull()), new ScNull()))),
            new ScPair('\u1E9Calist', new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Ctautology-checker', new ScPair('\u1E9Cx', new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Ctautologyp',
          new ScPair(new ScPair('\u1E9Cnormalize', new ScPair('\u1E9Cx', new ScNull())), new ScPair(new ScPair('\u1E9Cnil', new ScNull()), new ScNull()))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cfalsify', new ScPair('\u1E9Cx', new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cfalsify1',
          new ScPair(new ScPair('\u1E9Cnormalize', new ScPair('\u1E9Cx', new ScNull())), new ScPair(new ScPair('\u1E9Cnil', new ScNull()), new ScNull()))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cprime', new ScPair('\u1E9Cx', new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Czerop', new ScPair('\u1E9Cx', new ScNull())), new ScNull())),
            new ScPair(
              new ScPair(
                '\u1E9Cnot',
                new ScPair(
                  new ScPair(
                    '\u1E9Cequal',
                    new ScPair(
                      '\u1E9Cx',
                      new ScPair(new ScPair('\u1E9Cadd1', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())), new ScNull())
                    )
                  ),
                  new ScNull()
                )
              ),
              new ScPair(
                new ScPair('\u1E9Cprime1', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Csub1', new ScPair('\u1E9Cx', new ScNull())), new ScNull()))),
                new ScNull()
              )
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cand', new ScPair('\u1E9Cp', new ScPair('\u1E9Cq', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            '\u1E9Cp',
            new ScPair(
              new ScPair(
                '\u1E9Cif',
                new ScPair('\u1E9Cq', new ScPair(new ScPair('\u1E9Ct', new ScNull()), new ScPair(new ScPair('\u1E9Cf', new ScNull()), new ScNull())))
              ),
              new ScPair(new ScPair('\u1E9Cf', new ScNull()), new ScNull())
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cor', new ScPair('\u1E9Cp', new ScPair('\u1E9Cq', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            '\u1E9Cp',
            new ScPair(
              new ScPair('\u1E9Ct', new ScNull()),
              new ScPair(
                new ScPair(
                  '\u1E9Cif',
                  new ScPair('\u1E9Cq', new ScPair(new ScPair('\u1E9Ct', new ScNull()), new ScPair(new ScPair('\u1E9Cf', new ScNull()), new ScNull())))
                ),
                new ScNull()
              )
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cnot', new ScPair('\u1E9Cp', new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair('\u1E9Cp', new ScPair(new ScPair('\u1E9Cf', new ScNull()), new ScPair(new ScPair('\u1E9Ct', new ScNull()), new ScNull())))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cimplies', new ScPair('\u1E9Cp', new ScPair('\u1E9Cq', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            '\u1E9Cp',
            new ScPair(
              new ScPair(
                '\u1E9Cif',
                new ScPair('\u1E9Cq', new ScPair(new ScPair('\u1E9Ct', new ScNull()), new ScPair(new ScPair('\u1E9Cf', new ScNull()), new ScNull())))
              ),
              new ScPair(new ScPair('\u1E9Ct', new ScNull()), new ScNull())
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cfix', new ScPair('\u1E9Cx', new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Cnumberp', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cif',
        new ScPair(
          new ScPair('\u1E9Cif', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScPair('\u1E9Cc', new ScNull())))),
          new ScPair('\u1E9Cd', new ScPair('\u1E9Ce', new ScNull()))
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            '\u1E9Ca',
            new ScPair(
              new ScPair('\u1E9Cif', new ScPair('\u1E9Cb', new ScPair('\u1E9Cd', new ScPair('\u1E9Ce', new ScNull())))),
              new ScPair(new ScPair('\u1E9Cif', new ScPair('\u1E9Cc', new ScPair('\u1E9Cd', new ScPair('\u1E9Ce', new ScNull())))), new ScNull())
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Czerop', new ScPair('\u1E9Cx', new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cor',
          new ScPair(
            new ScPair('\u1E9Cequal', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))),
            new ScPair(new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Cnumberp', new ScPair('\u1E9Cx', new ScNull())), new ScNull())), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cplus',
        new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScPair('\u1E9Cz', new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cplus',
          new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))), new ScNull()))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(
          new ScPair('\u1E9Cplus', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))),
          new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Czerop', new ScPair('\u1E9Ca', new ScNull())),
            new ScPair(new ScPair('\u1E9Czerop', new ScPair('\u1E9Cb', new ScNull())), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cdifference', new ScPair('\u1E9Cx', new ScPair('\u1E9Cx', new ScNull()))),
      new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(
          new ScPair('\u1E9Cplus', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))),
          new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Ca', new ScPair('\u1E9Cc', new ScNull()))), new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cequal',
          new ScPair(
            new ScPair('\u1E9Cfix', new ScPair('\u1E9Cb', new ScNull())),
            new ScPair(new ScPair('\u1E9Cfix', new ScPair('\u1E9Cc', new ScNull())), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(
          new ScPair('\u1E9Czero', new ScNull()),
          new ScPair(new ScPair('\u1E9Cdifference', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())
        )
      ),
      new ScPair(
        new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cy', new ScPair('\u1E9Cx', new ScNull()))), new ScNull())),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cdifference', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Cnumberp', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair(
              new ScPair(
                '\u1E9Cor',
                new ScPair(
                  new ScPair('\u1E9Cequal', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))),
                  new ScPair(new ScPair('\u1E9Czerop', new ScPair('\u1E9Cy', new ScNull())), new ScNull())
                )
              ),
              new ScNull()
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cmeaning',
        new ScPair(
          new ScPair('\u1E9Cplus-tree', new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())),
          new ScPair('\u1E9Ca', new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cplus',
          new ScPair(
            new ScPair(
              '\u1E9Cmeaning',
              new ScPair(new ScPair('\u1E9Cplus-tree', new ScPair('\u1E9Cx', new ScNull())), new ScPair('\u1E9Ca', new ScNull()))
            ),
            new ScPair(
              new ScPair(
                '\u1E9Cmeaning',
                new ScPair(new ScPair('\u1E9Cplus-tree', new ScPair('\u1E9Cy', new ScNull())), new ScPair('\u1E9Ca', new ScNull()))
              ),
              new ScNull()
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cmeaning',
        new ScPair(
          new ScPair('\u1E9Cplus-tree', new ScPair(new ScPair('\u1E9Cplus-fringe', new ScPair('\u1E9Cx', new ScNull())), new ScNull())),
          new ScPair('\u1E9Ca', new ScNull())
        )
      ),
      new ScPair(
        new ScPair('\u1E9Cfix', new ScPair(new ScPair('\u1E9Cmeaning', new ScPair('\u1E9Cx', new ScPair('\u1E9Ca', new ScNull()))), new ScNull())),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cappend',
        new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScPair('\u1E9Cz', new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cappend',
          new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))), new ScNull()))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Creverse', new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))), new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cappend',
          new ScPair(
            new ScPair('\u1E9Creverse', new ScPair('\u1E9Cb', new ScNull())),
            new ScPair(new ScPair('\u1E9Creverse', new ScPair('\u1E9Ca', new ScNull())), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Ctimes',
        new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cplus',
          new ScPair(
            new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
            new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cx', new ScPair('\u1E9Cz', new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Ctimes',
        new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScPair('\u1E9Cz', new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Ctimes',
          new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))), new ScNull()))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(
          new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
          new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cor',
          new ScPair(
            new ScPair('\u1E9Czerop', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair(new ScPair('\u1E9Czerop', new ScPair('\u1E9Cy', new ScNull())), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cexec',
        new ScPair(
          new ScPair('\u1E9Cappend', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
          new ScPair('\u1E9Cpds', new ScPair('\u1E9Cenvrn', new ScNull()))
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cexec',
          new ScPair(
            '\u1E9Cy',
            new ScPair(
              new ScPair('\u1E9Cexec', new ScPair('\u1E9Cx', new ScPair('\u1E9Cpds', new ScPair('\u1E9Cenvrn', new ScNull())))),
              new ScPair('\u1E9Cenvrn', new ScNull())
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cmc-flatten', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(
        new ScPair('\u1E9Cappend', new ScPair(new ScPair('\u1E9Cflatten', new ScPair('\u1E9Cx', new ScNull())), new ScPair('\u1E9Cy', new ScNull()))),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cmember',
        new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cor',
          new ScPair(
            new ScPair('\u1E9Cmember', new ScPair('\u1E9Cx', new ScPair('\u1E9Ca', new ScNull()))),
            new ScPair(new ScPair('\u1E9Cmember', new ScPair('\u1E9Cx', new ScPair('\u1E9Cb', new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cmember', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Creverse', new ScPair('\u1E9Cy', new ScNull())), new ScNull()))),
      new ScPair(new ScPair('\u1E9Cmember', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Clength', new ScPair(new ScPair('\u1E9Creverse', new ScPair('\u1E9Cx', new ScNull())), new ScNull())),
      new ScPair(new ScPair('\u1E9Clength', new ScPair('\u1E9Cx', new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cmember',
        new ScPair('\u1E9Ca', new ScPair(new ScPair('\u1E9Cintersect', new ScPair('\u1E9Cb', new ScPair('\u1E9Cc', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Cmember', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))),
            new ScPair(new ScPair('\u1E9Cmember', new ScPair('\u1E9Ca', new ScPair('\u1E9Cc', new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cnth', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScPair('\u1E9Ci', new ScNull()))),
      new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cexp',
        new ScPair('\u1E9Ci', new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cj', new ScPair('\u1E9Ck', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Ctimes',
          new ScPair(
            new ScPair('\u1E9Cexp', new ScPair('\u1E9Ci', new ScPair('\u1E9Cj', new ScNull()))),
            new ScPair(new ScPair('\u1E9Cexp', new ScPair('\u1E9Ci', new ScPair('\u1E9Ck', new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cexp',
        new ScPair('\u1E9Ci', new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cj', new ScPair('\u1E9Ck', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cexp',
          new ScPair(new ScPair('\u1E9Cexp', new ScPair('\u1E9Ci', new ScPair('\u1E9Cj', new ScNull()))), new ScPair('\u1E9Ck', new ScNull()))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Creverse-loop', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(
        new ScPair('\u1E9Cappend', new ScPair(new ScPair('\u1E9Creverse', new ScPair('\u1E9Cx', new ScNull())), new ScPair('\u1E9Cy', new ScNull()))),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Creverse-loop', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cnil', new ScNull()), new ScNull()))),
      new ScPair(new ScPair('\u1E9Creverse', new ScPair('\u1E9Cx', new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Ccount-list',
        new ScPair('\u1E9Cz', new ScPair(new ScPair('\u1E9Csort-lp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cplus',
          new ScPair(
            new ScPair('\u1E9Ccount-list', new ScPair('\u1E9Cz', new ScPair('\u1E9Cx', new ScNull()))),
            new ScPair(new ScPair('\u1E9Ccount-list', new ScPair('\u1E9Cz', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(
          new ScPair('\u1E9Cappend', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))),
          new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Ca', new ScPair('\u1E9Cc', new ScNull()))), new ScNull())
        )
      ),
      new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cb', new ScPair('\u1E9Cc', new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cplus',
        new ScPair(
          new ScPair('\u1E9Cremainder', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
          new ScPair(
            new ScPair(
              '\u1E9Ctimes',
              new ScPair('\u1E9Cy', new ScPair(new ScPair('\u1E9Cquotient', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull()))
            ),
            new ScNull()
          )
        )
      ),
      new ScPair(new ScPair('\u1E9Cfix', new ScPair('\u1E9Cx', new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cpower-eval',
        new ScPair(
          new ScPair('\u1E9Cbig-plus1', new ScPair('\u1E9Cl', new ScPair('\u1E9Ci', new ScPair('\u1E9Cbase', new ScNull())))),
          new ScPair('\u1E9Cbase', new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cplus',
          new ScPair(new ScPair('\u1E9Cpower-eval', new ScPair('\u1E9Cl', new ScPair('\u1E9Cbase', new ScNull()))), new ScPair('\u1E9Ci', new ScNull()))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cpower-eval',
        new ScPair(
          new ScPair('\u1E9Cbig-plus', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScPair('\u1E9Ci', new ScPair('\u1E9Cbase', new ScNull()))))),
          new ScPair('\u1E9Cbase', new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cplus',
          new ScPair(
            '\u1E9Ci',
            new ScPair(
              new ScPair(
                '\u1E9Cplus',
                new ScPair(
                  new ScPair('\u1E9Cpower-eval', new ScPair('\u1E9Cx', new ScPair('\u1E9Cbase', new ScNull()))),
                  new ScPair(new ScPair('\u1E9Cpower-eval', new ScPair('\u1E9Cy', new ScPair('\u1E9Cbase', new ScNull()))), new ScNull())
                )
              ),
              new ScNull()
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cremainder', new ScPair('\u1E9Cy', new ScPair(1, new ScNull()))),
      new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Clessp',
        new ScPair(new ScPair('\u1E9Cremainder', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScPair('\u1E9Cy', new ScNull()))
      ),
      new ScPair(new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Czerop', new ScPair('\u1E9Cy', new ScNull())), new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cremainder', new ScPair('\u1E9Cx', new ScPair('\u1E9Cx', new ScNull()))),
      new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Clessp',
        new ScPair(new ScPair('\u1E9Cquotient', new ScPair('\u1E9Ci', new ScPair('\u1E9Cj', new ScNull()))), new ScPair('\u1E9Ci', new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Czerop', new ScPair('\u1E9Ci', new ScNull())), new ScNull())),
            new ScPair(
              new ScPair(
                '\u1E9Cor',
                new ScPair(
                  new ScPair('\u1E9Czerop', new ScPair('\u1E9Cj', new ScNull())),
                  new ScPair(
                    new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cj', new ScPair(1, new ScNull()))), new ScNull())),
                    new ScNull()
                  )
                )
              ),
              new ScNull()
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Clessp',
        new ScPair(new ScPair('\u1E9Cremainder', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScPair('\u1E9Cx', new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Czerop', new ScPair('\u1E9Cy', new ScNull())), new ScNull())),
            new ScPair(
              new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Czerop', new ScPair('\u1E9Cx', new ScNull())), new ScNull())),
              new ScPair(
                new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())),
                new ScNull()
              )
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cpower-eval',
        new ScPair(new ScPair('\u1E9Cpower-rep', new ScPair('\u1E9Ci', new ScPair('\u1E9Cbase', new ScNull()))), new ScPair('\u1E9Cbase', new ScNull()))
      ),
      new ScPair(new ScPair('\u1E9Cfix', new ScPair('\u1E9Ci', new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cpower-eval',
        new ScPair(
          new ScPair(
            '\u1E9Cbig-plus',
            new ScPair(
              new ScPair('\u1E9Cpower-rep', new ScPair('\u1E9Ci', new ScPair('\u1E9Cbase', new ScNull()))),
              new ScPair(
                new ScPair('\u1E9Cpower-rep', new ScPair('\u1E9Cj', new ScPair('\u1E9Cbase', new ScNull()))),
                new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScPair('\u1E9Cbase', new ScNull()))
              )
            )
          ),
          new ScPair('\u1E9Cbase', new ScNull())
        )
      ),
      new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Ci', new ScPair('\u1E9Cj', new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cgcd', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(new ScPair('\u1E9Cgcd', new ScPair('\u1E9Cy', new ScPair('\u1E9Cx', new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cnth',
        new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))), new ScPair('\u1E9Ci', new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cappend',
          new ScPair(
            new ScPair('\u1E9Cnth', new ScPair('\u1E9Ca', new ScPair('\u1E9Ci', new ScNull()))),
            new ScPair(
              new ScPair(
                '\u1E9Cnth',
                new ScPair(
                  '\u1E9Cb',
                  new ScPair(
                    new ScPair(
                      '\u1E9Cdifference',
                      new ScPair('\u1E9Ci', new ScPair(new ScPair('\u1E9Clength', new ScPair('\u1E9Ca', new ScNull())), new ScNull()))
                    ),
                    new ScNull()
                  )
                )
              ),
              new ScNull()
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cdifference',
        new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScPair('\u1E9Cx', new ScNull()))
      ),
      new ScPair(new ScPair('\u1E9Cfix', new ScPair('\u1E9Cy', new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cdifference',
        new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cy', new ScPair('\u1E9Cx', new ScNull()))), new ScPair('\u1E9Cx', new ScNull()))
      ),
      new ScPair(new ScPair('\u1E9Cfix', new ScPair('\u1E9Cy', new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cdifference',
        new ScPair(
          new ScPair('\u1E9Cplus', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
          new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cx', new ScPair('\u1E9Cz', new ScNull()))), new ScNull())
        )
      ),
      new ScPair(new ScPair('\u1E9Cdifference', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Ctimes',
        new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cdifference', new ScPair('\u1E9Cc', new ScPair('\u1E9Cw', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cdifference',
          new ScPair(
            new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cc', new ScPair('\u1E9Cx', new ScNull()))),
            new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cw', new ScPair('\u1E9Cx', new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cremainder',
        new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cx', new ScPair('\u1E9Cz', new ScNull()))), new ScPair('\u1E9Cz', new ScNull()))
      ),
      new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cdifference',
        new ScPair(
          new ScPair(
            '\u1E9Cplus',
            new ScPair('\u1E9Cb', new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Ca', new ScPair('\u1E9Cc', new ScNull()))), new ScNull()))
          ),
          new ScPair('\u1E9Ca', new ScNull())
        )
      ),
      new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cb', new ScPair('\u1E9Cc', new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cdifference',
        new ScPair(
          new ScPair('\u1E9Cadd1', new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))), new ScNull())),
          new ScPair('\u1E9Cz', new ScNull())
        )
      ),
      new ScPair(new ScPair('\u1E9Cadd1', new ScPair('\u1E9Cy', new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Clessp',
        new ScPair(
          new ScPair('\u1E9Cplus', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
          new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cx', new ScPair('\u1E9Cz', new ScNull()))), new ScNull())
        )
      ),
      new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Clessp',
        new ScPair(
          new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cx', new ScPair('\u1E9Cz', new ScNull()))),
          new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))), new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Czerop', new ScPair('\u1E9Cz', new ScNull())), new ScNull())),
            new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Clessp',
        new ScPair('\u1E9Cy', new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull()))
      ),
      new ScPair(new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Czerop', new ScPair('\u1E9Cx', new ScNull())), new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cgcd',
        new ScPair(
          new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cx', new ScPair('\u1E9Cz', new ScNull()))),
          new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))), new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Ctimes',
          new ScPair('\u1E9Cz', new ScPair(new ScPair('\u1E9Cgcd', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull()))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cvalue', new ScPair(new ScPair('\u1E9Cnormalize', new ScPair('\u1E9Cx', new ScNull())), new ScPair('\u1E9Ca', new ScNull()))),
      new ScPair(new ScPair('\u1E9Cvalue', new ScPair('\u1E9Cx', new ScPair('\u1E9Ca', new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(
          new ScPair('\u1E9Cflatten', new ScPair('\u1E9Cx', new ScNull())),
          new ScPair(new ScPair('\u1E9Ccons', new ScPair('\u1E9Cy', new ScPair(new ScPair('\u1E9Cnil', new ScNull()), new ScNull()))), new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Cnlistp', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Clistp', new ScPair(new ScPair('\u1E9Cgopher', new ScPair('\u1E9Cx', new ScNull())), new ScNull())),
      new ScPair(new ScPair('\u1E9Clistp', new ScPair('\u1E9Cx', new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Csamefringe', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Cequal',
          new ScPair(
            new ScPair('\u1E9Cflatten', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair(new ScPair('\u1E9Cflatten', new ScPair('\u1E9Cy', new ScNull())), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(
          new ScPair('\u1E9Cgreatest-factor', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
          new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair(
              '\u1E9Cor',
              new ScPair(
                new ScPair('\u1E9Czerop', new ScPair('\u1E9Cy', new ScNull())),
                new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cy', new ScPair(1, new ScNull()))), new ScNull())
              )
            ),
            new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(new ScPair('\u1E9Cgreatest-factor', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScPair(1, new ScNull()))
      ),
      new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cx', new ScPair(1, new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cnumberp',
        new ScPair(new ScPair('\u1E9Cgreatest-factor', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cnot',
          new ScPair(
            new ScPair(
              '\u1E9Cand',
              new ScPair(
                new ScPair(
                  '\u1E9Cor',
                  new ScPair(
                    new ScPair('\u1E9Czerop', new ScPair('\u1E9Cy', new ScNull())),
                    new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cy', new ScPair(1, new ScNull()))), new ScNull())
                  )
                ),
                new ScPair(
                  new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Cnumberp', new ScPair('\u1E9Cx', new ScNull())), new ScNull())),
                  new ScNull()
                )
              )
            ),
            new ScNull()
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Ctimes-list', new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Ctimes',
          new ScPair(
            new ScPair('\u1E9Ctimes-list', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair(new ScPair('\u1E9Ctimes-list', new ScPair('\u1E9Cy', new ScNull())), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cprime-list', new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Cprime-list', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair(new ScPair('\u1E9Cprime-list', new ScPair('\u1E9Cy', new ScNull())), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair('\u1E9Cz', new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cw', new ScPair('\u1E9Cz', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cand',
          new ScPair(
            new ScPair('\u1E9Cnumberp', new ScPair('\u1E9Cz', new ScNull())),
            new ScPair(
              new ScPair(
                '\u1E9Cor',
                new ScPair(
                  new ScPair('\u1E9Cequal', new ScPair('\u1E9Cz', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))),
                  new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cw', new ScPair(1, new ScNull()))), new ScNull())
                )
              ),
              new ScNull()
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cgreatereqp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
      new ScPair(
        new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cor',
          new ScPair(
            new ScPair('\u1E9Cequal', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))),
            new ScPair(
              new ScPair(
                '\u1E9Cand',
                new ScPair(
                  new ScPair('\u1E9Cnumberp', new ScPair('\u1E9Cx', new ScNull())),
                  new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cy', new ScPair(1, new ScNull()))), new ScNull())
                )
              ),
              new ScNull()
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cremainder',
        new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cy', new ScPair('\u1E9Cx', new ScNull()))), new ScPair('\u1E9Cy', new ScNull()))
      ),
      new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))), new ScPair(1, new ScNull()))
      ),
      new ScPair(
        scList(
          '\u1E9Cand',
          new ScPair(
            '\u1E9Cnot',
            new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Ca', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))), new ScNull())
          ),
          new ScPair(
            '\u1E9Cnot',
            new ScPair(new ScPair('\u1E9Cequal', new ScPair('\u1E9Cb', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))), new ScNull())
          ),
          new ScPair('\u1E9Cnumberp', new ScPair('\u1E9Ca', new ScNull())),
          new ScPair('\u1E9Cnumberp', new ScPair('\u1E9Cb', new ScNull())),
          new ScPair(
            '\u1E9Cequal',
            new ScPair(new ScPair('\u1E9Csub1', new ScPair('\u1E9Ca', new ScNull())), new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))
          ),
          new ScPair(
            '\u1E9Cequal',
            new ScPair(new ScPair('\u1E9Csub1', new ScPair('\u1E9Cb', new ScNull())), new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))
          )
        ),
        new ScNull()
      )
    )
  ) as ScPair,
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Clessp',
        new ScPair(
          new ScPair('\u1E9Clength', new ScPair(new ScPair('\u1E9Cdelete', new ScPair('\u1E9Cx', new ScPair('\u1E9Cl', new ScNull()))), new ScNull())),
          new ScPair(new ScPair('\u1E9Clength', new ScPair('\u1E9Cl', new ScNull())), new ScNull())
        )
      ),
      new ScPair(new ScPair('\u1E9Cmember', new ScPair('\u1E9Cx', new ScPair('\u1E9Cl', new ScNull()))), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Csort2', new ScPair(new ScPair('\u1E9Cdelete', new ScPair('\u1E9Cx', new ScPair('\u1E9Cl', new ScNull()))), new ScNull())),
      new ScPair(
        new ScPair('\u1E9Cdelete', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Csort2', new ScPair('\u1E9Cl', new ScNull())), new ScNull()))),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cdsort', new ScPair('\u1E9Cx', new ScNull())),
      new ScPair(new ScPair('\u1E9Csort2', new ScPair('\u1E9Cx', new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Clength',
        new ScPair(
          new ScPair(
            '\u1E9Ccons',
            new ScPair(
              '\u1E9Cx1',
              new ScPair(
                new ScPair(
                  '\u1E9Ccons',
                  new ScPair(
                    '\u1E9Cx2',
                    new ScPair(
                      new ScPair(
                        '\u1E9Ccons',
                        new ScPair(
                          '\u1E9Cx3',
                          new ScPair(
                            new ScPair(
                              '\u1E9Ccons',
                              new ScPair(
                                '\u1E9Cx4',
                                new ScPair(
                                  new ScPair(
                                    '\u1E9Ccons',
                                    new ScPair(
                                      '\u1E9Cx5',
                                      new ScPair(new ScPair('\u1E9Ccons', new ScPair('\u1E9Cx6', new ScPair('\u1E9Cx7', new ScNull()))), new ScNull())
                                    )
                                  ),
                                  new ScNull()
                                )
                              )
                            ),
                            new ScNull()
                          )
                        )
                      ),
                      new ScNull()
                    )
                  )
                ),
                new ScNull()
              )
            )
          ),
          new ScNull()
        )
      ),
      new ScPair(
        new ScPair('\u1E9Cplus', new ScPair(EARLEYBOYER_6, new ScPair(new ScPair('\u1E9Clength', new ScPair('\u1E9Cx7', new ScNull())), new ScNull()))),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cdifference',
        new ScPair(
          new ScPair('\u1E9Cadd1', new ScPair(new ScPair('\u1E9Cadd1', new ScPair('\u1E9Cx', new ScNull())), new ScNull())),
          new ScPair(EARLEYBOYER_2, new ScNull())
        )
      ),
      new ScPair(new ScPair('\u1E9Cfix', new ScPair('\u1E9Cx', new ScNull())), new ScNull())
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cquotient',
        new ScPair(
          new ScPair(
            '\u1E9Cplus',
            new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull()))
          ),
          new ScPair(EARLEYBOYER_2, new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cplus',
          new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cquotient', new ScPair('\u1E9Cy', new ScPair(EARLEYBOYER_2, new ScNull()))), new ScNull()))
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Csigma', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScPair('\u1E9Ci', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Cquotient',
          new ScPair(
            new ScPair('\u1E9Ctimes', new ScPair('\u1E9Ci', new ScPair(new ScPair('\u1E9Cadd1', new ScPair('\u1E9Ci', new ScNull())), new ScNull()))),
            new ScPair(EARLEYBOYER_2, new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cplus', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cadd1', new ScPair('\u1E9Cy', new ScNull())), new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Cnumberp', new ScPair('\u1E9Cy', new ScNull())),
            new ScPair(
              new ScPair('\u1E9Cadd1', new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())),
              new ScPair(new ScPair('\u1E9Cadd1', new ScPair('\u1E9Cx', new ScNull())), new ScNull())
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(
          new ScPair('\u1E9Cdifference', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
          new ScPair(new ScPair('\u1E9Cdifference', new ScPair('\u1E9Cz', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Clessp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
            new ScPair(
              new ScPair('\u1E9Cnot', new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cy', new ScPair('\u1E9Cz', new ScNull()))), new ScNull())),
              new ScPair(
                new ScPair(
                  '\u1E9Cif',
                  new ScPair(
                    new ScPair('\u1E9Clessp', new ScPair('\u1E9Cz', new ScPair('\u1E9Cy', new ScNull()))),
                    new ScPair(
                      new ScPair(
                        '\u1E9Cnot',
                        new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cy', new ScPair('\u1E9Cx', new ScNull()))), new ScNull())
                      ),
                      new ScPair(
                        new ScPair(
                          '\u1E9Cequal',
                          new ScPair(
                            new ScPair('\u1E9Cfix', new ScPair('\u1E9Cx', new ScNull())),
                            new ScPair(new ScPair('\u1E9Cfix', new ScPair('\u1E9Cz', new ScNull())), new ScNull())
                          )
                        ),
                        new ScNull()
                      )
                    )
                  )
                ),
                new ScNull()
              )
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cmeaning',
        new ScPair(
          new ScPair('\u1E9Cplus-tree', new ScPair(new ScPair('\u1E9Cdelete', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())),
          new ScPair('\u1E9Ca', new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Cmember', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
            new ScPair(
              new ScPair(
                '\u1E9Cdifference',
                new ScPair(
                  new ScPair(
                    '\u1E9Cmeaning',
                    new ScPair(new ScPair('\u1E9Cplus-tree', new ScPair('\u1E9Cy', new ScNull())), new ScPair('\u1E9Ca', new ScNull()))
                  ),
                  new ScPair(new ScPair('\u1E9Cmeaning', new ScPair('\u1E9Cx', new ScPair('\u1E9Ca', new ScNull()))), new ScNull())
                )
              ),
              new ScPair(
                new ScPair(
                  '\u1E9Cmeaning',
                  new ScPair(new ScPair('\u1E9Cplus-tree', new ScPair('\u1E9Cy', new ScNull())), new ScPair('\u1E9Ca', new ScNull()))
                ),
                new ScNull()
              )
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cadd1', new ScPair('\u1E9Cy', new ScNull())), new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Cnumberp', new ScPair('\u1E9Cy', new ScNull())),
            new ScPair(
              new ScPair(
                '\u1E9Cplus',
                new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull()))
              ),
              new ScPair(new ScPair('\u1E9Cfix', new ScPair('\u1E9Cx', new ScNull())), new ScNull())
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Cnth', new ScPair(new ScPair('\u1E9Cnil', new ScNull()), new ScPair('\u1E9Ci', new ScNull()))),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Czerop', new ScPair('\u1E9Ci', new ScNull())),
            new ScPair(new ScPair('\u1E9Cnil', new ScNull()), new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Clast', new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))), new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Clistp', new ScPair('\u1E9Cb', new ScNull())),
            new ScPair(
              new ScPair('\u1E9Clast', new ScPair('\u1E9Cb', new ScNull())),
              new ScPair(
                new ScPair(
                  '\u1E9Cif',
                  new ScPair(
                    new ScPair('\u1E9Clistp', new ScPair('\u1E9Ca', new ScNull())),
                    new ScPair(
                      new ScPair(
                        '\u1E9Ccons',
                        new ScPair(
                          new ScPair('\u1E9Ccar', new ScPair(new ScPair('\u1E9Clast', new ScPair('\u1E9Ca', new ScNull())), new ScNull())),
                          new ScPair('\u1E9Cb', new ScNull())
                        )
                      ),
                      new ScPair('\u1E9Cb', new ScNull())
                    )
                  )
                ),
                new ScNull()
              )
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cequal',
        new ScPair(new ScPair('\u1E9Clessp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScPair('\u1E9Cz', new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Clessp', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))),
            new ScPair(
              new ScPair('\u1E9Cequal', new ScPair(new ScPair('\u1E9Ct', new ScNull()), new ScPair('\u1E9Cz', new ScNull()))),
              new ScPair(new ScPair('\u1E9Cequal', new ScPair(new ScPair('\u1E9Cf', new ScNull()), new ScPair('\u1E9Cz', new ScNull()))), new ScNull())
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cassignment',
        new ScPair('\u1E9Cx', new ScPair(new ScPair('\u1E9Cappend', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))), new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Cassignedp', new ScPair('\u1E9Cx', new ScPair('\u1E9Ca', new ScNull()))),
            new ScPair(
              new ScPair('\u1E9Cassignment', new ScPair('\u1E9Cx', new ScPair('\u1E9Ca', new ScNull()))),
              new ScPair(new ScPair('\u1E9Cassignment', new ScPair('\u1E9Cx', new ScPair('\u1E9Cb', new ScNull()))), new ScNull())
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair('\u1E9Ccar', new ScPair(new ScPair('\u1E9Cgopher', new ScPair('\u1E9Cx', new ScNull())), new ScNull())),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Clistp', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair(
              new ScPair('\u1E9Ccar', new ScPair(new ScPair('\u1E9Cflatten', new ScPair('\u1E9Cx', new ScNull())), new ScNull())),
              new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull())
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cflatten',
        new ScPair(new ScPair('\u1E9Ccdr', new ScPair(new ScPair('\u1E9Cgopher', new ScPair('\u1E9Cx', new ScNull())), new ScNull())), new ScNull())
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Clistp', new ScPair('\u1E9Cx', new ScNull())),
            new ScPair(
              new ScPair('\u1E9Ccdr', new ScPair(new ScPair('\u1E9Cflatten', new ScPair('\u1E9Cx', new ScNull())), new ScNull())),
              new ScPair(
                new ScPair(
                  '\u1E9Ccons',
                  new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScPair(new ScPair('\u1E9Cnil', new ScNull()), new ScNull()))
                ),
                new ScNull()
              )
            )
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cquotient',
        new ScPair(new ScPair('\u1E9Ctimes', new ScPair('\u1E9Cy', new ScPair('\u1E9Cx', new ScNull()))), new ScPair('\u1E9Cy', new ScNull()))
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Czerop', new ScPair('\u1E9Cy', new ScNull())),
            new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScPair(new ScPair('\u1E9Cfix', new ScPair('\u1E9Cx', new ScNull())), new ScNull()))
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    '\u1E9Cequal',
    new ScPair(
      new ScPair(
        '\u1E9Cget',
        new ScPair(
          '\u1E9Cj',
          new ScPair(new ScPair('\u1E9Cset', new ScPair('\u1E9Ci', new ScPair('\u1E9Cval', new ScPair('\u1E9Cmem', new ScNull())))), new ScNull())
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cif',
          new ScPair(
            new ScPair('\u1E9Ceqp', new ScPair('\u1E9Cj', new ScPair('\u1E9Ci', new ScNull()))),
            new ScPair('\u1E9Cval', new ScPair(new ScPair('\u1E9Cget', new ScPair('\u1E9Cj', new ScPair('\u1E9Cmem', new ScNull()))), new ScNull()))
          )
        ),
        new ScNull()
      )
    )
  )
);
let constNboyer = new ScPair(
  new ScPair(
    '\u1E9Cx',
    new ScPair(
      '\u1E9Cf',
      new ScPair(
        new ScPair(
          '\u1E9Cplus',
          new ScPair(
            new ScPair('\u1E9Cplus', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))),
            new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cc', new ScPair(new ScPair('\u1E9Czero', new ScNull()), new ScNull()))), new ScNull())
          )
        ),
        new ScNull()
      )
    )
  ),
  new ScPair(
    new ScPair(
      '\u1E9Cy',
      new ScPair(
        '\u1E9Cf',
        new ScPair(
          new ScPair(
            '\u1E9Ctimes',
            new ScPair(
              new ScPair('\u1E9Ctimes', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))),
              new ScPair(new ScPair('\u1E9Cplus', new ScPair('\u1E9Cc', new ScPair('\u1E9Cd', new ScNull()))), new ScNull())
            )
          ),
          new ScNull()
        )
      )
    ),
    new ScPair(
      new ScPair(
        '\u1E9Cz',
        new ScPair(
          '\u1E9Cf',
          new ScPair(
            new ScPair(
              '\u1E9Creverse',
              new ScPair(
                new ScPair(
                  '\u1E9Cappend',
                  new ScPair(
                    new ScPair('\u1E9Cappend', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))),
                    new ScPair(new ScPair('\u1E9Cnil', new ScNull()), new ScNull())
                  )
                ),
                new ScNull()
              )
            ),
            new ScNull()
          )
        )
      ),
      new ScPair(
        new ScPair(
          '\u1E9Cu',
          new ScPair(
            '\u1E9Cequal',
            new ScPair(
              new ScPair('\u1E9Cplus', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))),
              new ScPair(new ScPair('\u1E9Cdifference', new ScPair('\u1E9Cx', new ScPair('\u1E9Cy', new ScNull()))), new ScNull())
            )
          )
        ),
        new ScPair(
          new ScPair(
            '\u1E9Cw',
            new ScPair(
              '\u1E9Clessp',
              new ScPair(
                new ScPair('\u1E9Cremainder', new ScPair('\u1E9Ca', new ScPair('\u1E9Cb', new ScNull()))),
                new ScPair(
                  new ScPair(
                    '\u1E9Cmember',
                    new ScPair('\u1E9Ca', new ScPair(new ScPair('\u1E9Clength', new ScPair('\u1E9Cb', new ScNull())), new ScNull()))
                  ),
                  new ScNull()
                )
              )
            )
          ),
          new ScNull()
        )
      )
    )
  )
);

let bgLScZa2symbolzd2recordszd2alistza22z00Nboyer: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
let ifConstructorNboyer: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = '\u1E9C*';
let falseTermNboyer: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
let trueTermNboyer: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
let rewriteCountNboyer: number = 0;
let unifySubstNboyer: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = '\u1E9C*';
let scNumber2string = scNumber2jsstring;

/**
 boyer
 */
function applySubstLstNboyer(
  alist: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  lst: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  let scLst7: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  if (lst instanceof ScNull) {
    return new ScNull();
  }
  scLst7 = (lst as ScPair).cdr;
  if (scLst7 instanceof ScNull) {
    return new ScPair(applySubstNboyer(alist, (lst as ScPair).car), new ScNull());
  } else {
    return new ScPair(
      applySubstNboyer(alist, (lst as ScPair).car),
      new ScPair(applySubstNboyer(alist, (scLst7 as ScPair).car), applySubstLstNboyer(alist, (scLst7 as ScPair).cdr))
    );
  }
}
function rewriteArgsNboyer(
  lst: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  let scLst14: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  if (lst instanceof ScNull) {
    return new ScNull();
  }
  let ranCar = rewriteNboyer((lst as ScPair).car);
  scLst14 = (lst as ScPair).cdr;
  if (scLst14 instanceof ScNull) {
    return new ScPair(ranCar, new ScNull());
  }
  return new ScPair(ranCar, new ScPair(rewriteNboyer((scLst14 as ScPair).car), rewriteArgsNboyer((scLst14 as ScPair).cdr)));
}
function translateAlistNboyer(
  alist: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  //debugLog('This is translateAlistNboyer');
  let scAlist6: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let term: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let carValue: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  if (alist instanceof ScNull) {
    return new ScNull();
  }
  term = ((alist as ScPair).car as ScPair).cdr;
  scAlist6 = (alist as ScPair).cdr;
  if (!(term instanceof ScPair)) {
    carValue = new ScPair(((alist as ScPair).car as ScPair).car, term);
  } else {
    carValue = new ScPair(
      ((alist as ScPair).car as ScPair).car,
      new ScPair(bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer((term as ScPair).car as string), translateArgsNboyer((term as ScPair).cdr))
    );
  }
  if (scAlist6 instanceof ScNull) {
    return new ScPair(carValue, new ScNull());
  } else {
    return new ScPair(
      carValue,
      new ScPair(
        new ScPair(((scAlist6 as ScPair).car as ScPair).car, translateTermNboyer(((scAlist6 as ScPair).car as ScPair).cdr)),
        translateAlistNboyer((scAlist6 as ScPair).cdr)
      )
    );
  }
}
function oneWayUnify1Nboyer(
  term1: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  term2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): boolean {
  let lst1: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let lst2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let tempTemp: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  if (!(term2 instanceof ScPair)) {
    tempTemp = scAssq(typeof term2 === 'string' ? term2 : String(term2 as number), unifySubstNboyer);
    if (tempTemp !== false) {
      return isTermEqualNboyer(term1, (tempTemp as ScPair).cdr);
    } else if (scIsNumber(term2)) {
      return scIsEqual(term1, term2);
    } else {
      unifySubstNboyer = new ScPair(new ScPair(term2, term1), unifySubstNboyer);
      return true;
    }
  } else if (!(term1 instanceof ScPair)) {
    return false;
  } else if ((term1 as ScPair).car === (term2 as ScPair).car) {
    lst1 = (term1 as ScPair).cdr;
    lst2 = (term2 as ScPair).cdr;
    while (true) {
      if (lst1 instanceof ScNull) {
        return lst2 instanceof ScNull;
      } else if (lst2 instanceof ScNull) {
        return false;
      } else if (oneWayUnify1Nboyer((lst1 as ScPair).car, (lst2 as ScPair).car) !== false) {
        lst1 = (lst1 as ScPair).cdr;
        lst2 = (lst2 as ScPair).cdr;
      } else {
        return false;
      }
    }
  } else {
    return false;
  }
}

function rewriteNboyer(
  term: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  let term2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let scTerm12: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let lst: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let symbolRecord: ScVector;
  let scLst13: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  rewriteCountNboyer = rewriteCountNboyer + 1;
  if (!(term instanceof ScPair)) {
    return term;
  } else {
    scLst13 = (term as ScPair).cdr;
    if (scLst13 instanceof ScNull) {
      scTerm12 = new ScPair((term as ScPair).car, new ScNull());
    } else {
      scTerm12 = new ScPair((term as ScPair).car, new ScPair(rewriteNboyer((scLst13 as ScPair).car), rewriteArgsNboyer((scLst13 as ScPair).cdr)));
    }
    symbolRecord = (term as ScPair).car as ScVector;
    lst = symbolRecord.arr[1];
    while (true) {
      if (lst instanceof ScNull) {
        return scTerm12;
      } else {
        term2 = (((lst as ScPair).car as ScPair).cdr as ScPair).car;
        unifySubstNboyer = new ScNull();
        if (oneWayUnify1Nboyer(scTerm12, term2) !== false) {
          return rewriteNboyer(applySubstNboyer(unifySubstNboyer, ((((lst as ScPair).car as ScPair).cdr as ScPair).cdr as ScPair).car));
        } else {
          lst = (lst as ScPair).cdr;
        }
      }
    }
  }
}

function tautologypNboyer(
  scx1: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  trueLst: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  falseLsta: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): boolean {
  //debugLog('This is tautologypNboyer')
  let tmp1125: boolean;
  let x: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let tmp1126: boolean;
  let scx8: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let scTmp11259: boolean;
  let scTmp112610: boolean;
  let scx11: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let falseLst: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  scx11 = scx1;
  falseLst = falseLsta;
  while (true) {
    scTmp112610 = isTermEqualNboyer(scx11, trueTermNboyer);
    scTmp11259 = isTermEqualNboyer(scx11, falseTermNboyer);
    if ((scTmp112610 !== false ? scTmp112610 : isTermMemberNboyer(scx11, trueLst)) !== false) {
      return true;
    } else if ((scTmp11259 !== false ? scTmp11259 : isTermMemberNboyer(scx11, falseLst)) !== false) {
      return false;
    } else if (!(scx11 instanceof ScPair)) {
      return false;
    } else if ((scx11 as ScPair).car === ifConstructorNboyer) {
      scx8 = ((scx11 as ScPair).cdr as ScPair).car;
      tmp1126 = isTermEqualNboyer(scx8, trueTermNboyer);
      x = ((scx11 as ScPair).cdr as ScPair).car;
      tmp1125 = isTermEqualNboyer(x, falseTermNboyer);
      if ((tmp1126 !== false ? tmp1126 : isTermMemberNboyer(scx8, trueLst)) !== false) {
        scx11 = (((scx11 as ScPair).cdr as ScPair).cdr as ScPair).car;
      } else if ((tmp1125 !== false ? tmp1125 : isTermMemberNboyer(x, falseLst)) !== false) {
        scx11 = ((((scx11 as ScPair).cdr as ScPair).cdr as ScPair).cdr as ScPair).car;
      } else if (
        tautologypNboyer((((scx11 as ScPair).cdr as ScPair).cdr as ScPair).car, new ScPair(((scx11 as ScPair).cdr as ScPair).car, trueLst), falseLst) !== false
      ) {
        falseLst = new ScPair(((scx11 as ScPair).cdr as ScPair).car, falseLst);
        scx11 = ((((scx11 as ScPair).cdr as ScPair).cdr as ScPair).cdr as ScPair).car;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}

function bgLSetupzd2Boyerzd2(): boolean {
  //debugLog('This is bgLSetupzd2Boyerzd2')
  let symbolRecord: ScVector;
  let value: ScPair;
  let bgLScSymbolzd2record16zd2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let sym: string;
  let scsym17: string;
  let term: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let lst: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let scTerm18: ScPair;
  let scTerm19: ScPair;
  let carValue: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  bgLScZa2symbolzd2recordszd2alistza22z00Nboyer = new ScNull();
  ifConstructorNboyer = bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer('\u1E9Cif');
  scTerm19 = new ScPair('\u1E9Cf', new ScNull());
  falseTermNboyer = new ScPair(bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer(scTerm19.car as string), translateArgsNboyer(scTerm19.cdr));
  scTerm18 = new ScPair('\u1E9Ct', new ScNull());
  trueTermNboyer = new ScPair(bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer(scTerm18.car as string), translateArgsNboyer(scTerm18.cdr));
  lst = scConst3Nboyer;
  while (!(lst instanceof ScNull)) {
    term = (lst as ScPair).car;
    if (term instanceof ScPair && term.car === '\u1E9Cequal' && (term.cdr as ScPair).car instanceof ScPair) {
      scsym17 = (((term as ScPair).cdr as ScPair).car as ScPair).car as string;
      carValue = new ScPair(bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer((term as ScPair).car as string), translateArgsNboyer((term as ScPair).cdr));
      sym = ((term.cdr as ScPair).car as ScPair).car as string;
      bgLScSymbolzd2record16zd2 = bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer(sym);
      value = new ScPair(carValue, (bgLScSymbolzd2record16zd2 as ScVector).arr[1]);
      symbolRecord = bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer(scsym17) as ScVector;
      symbolRecord.arr[1] = value as ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    } else {
      return false;
    }
    lst = (lst as ScPair).cdr;
  }
  return true;
}

function bgLTestzd2Boyerzd2(n: number): number | boolean {
  //debugLog('This is bgLTestzd2Boyerzd2')
  let optrOpnd: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let term: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let scn20: number;
  let answer: boolean;
  let scTerm21: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let scTerm22: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  rewriteCountNboyer = 0;
  term = scConst4Nboyer;
  scn20 = n;
  while (scn20 !== 0) {
    term = scList('\u{1E9C}', term as ScPair, new ScPair('\u{1E9C}f', new ScNull()));
    scn20 -= 1;
  }
  scTerm22 = term;
  if (!(scTerm22 instanceof ScPair)) {
    optrOpnd = scTerm22;
  } else {
    optrOpnd = new ScPair(bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer((scTerm22 as ScPair).car as string), translateArgsNboyer((scTerm22 as ScPair).cdr));
  }
  if (constNboyer instanceof ScNull) {
    scTerm21 = applySubstNboyer(new ScNull(), optrOpnd);
  } else {
    scTerm21 = applySubstNboyer(
      new ScPair(new ScPair((constNboyer.car as ScPair).car, translateTermNboyer((constNboyer.car as ScPair).cdr)), translateAlistNboyer(constNboyer.cdr)),
      optrOpnd
    );
  }
  answer = tautologypNboyer(rewriteNboyer(scTerm21), new ScNull(), new ScNull());
  scWrite(rewriteCountNboyer, new ScNull());
  scDisplay(' rewrites', new ScNull());
  if (answer !== false) {
    return rewriteCountNboyer;
  } else {
    return false;
  }
}

function warn(rewrites: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector): boolean {
  let rewritesValue: number;
  if (scIsNumber(rewrites)) {
    rewritesValue = rewrites as number;
    if (rewritesValue === EARLEYBOYER_95024) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function run(): number {
  return bgLTestzd2Boyerzd2(0) as number;
}

function bgNboyerzd2Benchmarkzd2(...argumentsAry: string[]): void {
  //debugLog("This is bgNboyerzd2Benchmarkzd2");
  let args: ScPair | undefined;
  let sctmp = argumentsAry.length - 1;
  while (sctmp >= 0) {
    args = scCons(argumentsAry[sctmp], args ?? new ScNull());
    sctmp -= 1;
  }
  let n = args === undefined ? 0 : args.car;
  bgLSetupzd2Boyerzd2();
  return bgLRunzd2Benchmarkzd2('nboyer' + scNumber2string(n as number), 1, run, warn);
}

/**
 earley
 */
function bgLMakezd2Parserzd2(
  grammar: ScPair,
  lexer: (lexer: ScPair) => ScPair
): (input: ScPair) => Array<ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector[]> {
  //debugLog('This is bgLMakezd2Parserzd2')
  let i: number;
  let parserDescr: Array<ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector[]>;
  let defLoop: (
    defs: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
    conf: number
  ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let nbNts: number;
  let names: ScVector;
  let steps: ScVector;
  let predictors: ScVector;
  let enders: ScVector;
  let starters: ScVector;
  let nts: string[];
  let scNames1: ScVector;
  let scSteps2: ScVector;
  let scPredictors3: ScVector;
  let scEnders4: ScVector;
  let scStarters5: ScVector;
  let nbConfs: number;
  let bgLScDefzd2Loop6zd2: (
    defs: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
    bgLScNbzd2confs14zd2: number
  ) => number;
  let bgLScNbzd2nts7zd2: number;
  let scNts8: string[];
  let bgLScDefzd2Loop9zd2: (
    defs: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
    scNts11: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
  ) => string[];
  bgLScDefzd2Loop9zd2 = (
    defs: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
    scNts11: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
  ): string[] => {
    //debugLog('This is bgLScDefzd2Loop9zd2')
    let ruleLoop: (rules: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector, scNts2: ScPair) => string[];
    let head: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let def: ScPair;
    if (defs instanceof ScPair) {
      def = (defs as ScPair).car as ScPair;
      head = def.car;
      ruleLoop = (rules: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector, scNts2: ScPair): string[] => {
        let nt: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
        let l: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
        let scNts13: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
        let rule: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
        if (rules instanceof ScPair) {
          rule = (rules as ScPair).car as ScPair;
          l = rule;
          scNts13 = scNts2;
          while (l instanceof ScPair) {
            nt = (l as ScPair).car;
            l = (l as ScPair).cdr;
            scNts13 = scMember(nt, scNts13) !== false ? scNts13 : new ScPair(nt, scNts13);
          }
          return ruleLoop((rules as ScPair).cdr, scNts13 as ScPair);
        } else {
          return bgLScDefzd2Loop9zd2((defs as ScPair).cdr, scNts2);
        }
      };
      return ruleLoop(def.cdr, scMember(head, scNts11) !== false ? (scNts11 as ScPair) : new ScPair(head, scNts11));
    } else {
      return scList2vector(scReverse(scNts11) as ScPair).arr as string[];
    }
  };
  scNts8 = bgLScDefzd2Loop9zd2(grammar, new ScNull());
  bgLScNbzd2nts7zd2 = scNts8.length;
  bgLScDefzd2Loop6zd2 = (
    defs: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
    bgLScNbzd2confs14zd2: number
  ): number => {
    //debugLog("This is bgLScDefzd2Loop6zd2")
    let ruleLoop: (
      rules: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
      bgLScNbzd2confs15zd2: number
    ) => number;
    let def: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    if (defs instanceof ScPair) {
      def = (defs as ScPair).car;
      ruleLoop = (rules: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
        bgLScNbzd2confs15zd2: number): number => {
        let l: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
        let bgLScNbzd2confs16zd2: number;
        let rule: ScPair;
        if (rules instanceof ScPair) {
          rule = (rules as ScPair).car as ScPair;
          l = rule;
          bgLScNbzd2confs16zd2 = bgLScNbzd2confs15zd2;
          while (l instanceof ScPair) {
            l = (l as ScPair).cdr as ScPair;
            bgLScNbzd2confs16zd2 += 1;
          }
          return ruleLoop((rules as ScPair).cdr, bgLScNbzd2confs16zd2 + 1);
        } else {
          return bgLScDefzd2Loop6zd2((defs as ScPair).cdr, bgLScNbzd2confs15zd2);
        }
      };
      return ruleLoop((def as ScPair).cdr, bgLScNbzd2confs14zd2);
    } else {
      return bgLScNbzd2confs14zd2;
    }
  };
  nbConfs = bgLScDefzd2Loop6zd2(grammar, 0) + bgLScNbzd2nts7zd2;
  scStarters5 = scMakeVector(bgLScNbzd2nts7zd2, new ScNull());
  scEnders4 = scMakeVector(bgLScNbzd2nts7zd2, new ScNull());
  scPredictors3 = scMakeVector(bgLScNbzd2nts7zd2, new ScNull());
  scSteps2 = scMakeVector(nbConfs, false);
  scNames1 = scMakeVector(nbConfs, false);
  nts = scNts8;
  starters = scStarters5;
  enders = scEnders4;
  predictors = scPredictors3;
  steps = scSteps2;
  names = scNames1;
  nbNts = scNts8.length;
  i = nbNts - 1;
  while (i >= 0) {
    scSteps2.arr[i] = i - nbNts;
    scNames1.arr[i] = scList(scNts8[i], 0);
    scEnders4.arr[i] = scList(i);
    i -= 1;
  }
  defLoop = (
    defs: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
    conf: number
  ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
    //debugLog('This is defLoop')
    let ruleLoop: (
      rules: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
      conf: number,
      ruleNum: number
    ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let head: string;
    let def: ScPair;
    if (defs instanceof ScPair) {
      def = (defs as ScPair).car as ScPair;
      head = def.car as string;
      ruleLoop = (
        rules: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
        conf: number,
        ruleNum: number
      ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
        let i: number;
        let sci17: number;
        let nt: string;
        let l: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
        let scConf18: number;
        let sci19: number;
        let rule: ScPair;
        if (rules instanceof ScPair) {
          rule = (rules as ScPair).car as ScPair;
          names.arr[conf] = scList(head, ruleNum);
          sci19 = ind(head, nts) as number;
          starters.arr[sci19] = new ScPair(conf, starters.arr[sci19]);
          l = rule;
          scConf18 = conf;
          while (l instanceof ScPair) {
            nt = (l as ScPair).car as string;
            steps.arr[scConf18] = ind(nt, nts);
            sci17 = ind(nt, nts) as number;
            predictors.arr[sci17] = new ScPair(scConf18, predictors.arr[sci17]);
            l = (l as ScPair).cdr;
            scConf18 += 1;
          }
          steps.arr[scConf18] = (ind(head, nts) as number) - nbNts;
          i = ind(head, nts) as number;
          enders.arr[i] = new ScPair(scConf18, enders.arr[i]);
          return ruleLoop(
            (rules as ScPair).cdr as ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
            scConf18 + 1,
            ruleNum + 1
          );
        } else {
          return defLoop((defs as ScPair).cdr as ScPair, conf);
        }
      };
      return ruleLoop(def.cdr, conf, 1);
    } else {
      return new ScNull();
    }
  };

  defLoop(grammar, scNts8.length);
  parserDescr = [lexer, scNts8, scStarters5, scEnders4, scPredictors3, scSteps2, scNames1];
  return input => {
    //debugLog('This is input')
    let optrOpnd: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let scOptrOpnd20: ScPair;
    let scOptrOpnd21: ScPair;
    let scOptrOpnd22: ScPair;
    let loop1: () => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let bgLScStateza223za2: ScVector;
    let toks: ScVector;
    let bgLScNbzd2nts24zd2: number;
    let scSteps25: ScVector;
    let scEnders26: ScVector;
    let stateNum: number;
    let bgLScStatesza227za2: ScVector;
    let states: ScVector;
    let i: number;
    let conf: number;
    let l: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let tokNts: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let sci28: number;
    let sci29: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let l1: ScPair;
    let l2: ScPair;
    let tok: ScPair;
    let tail1129: ScPair;
    let l1125: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let goalEnders: ScPair;
    let bgLScStatesza230za2: ScVector;
    let bgLScNbzd2nts31zd2: number;
    let bgLScNbzd2confs32zd2: number;
    let nbToks: number;
    let goalStarters: ScPair;
    let scStates33: ScVector;
    let bgLScNbzd2confs34zd2: number;
    let bgLScNbzd2toks35zd2: number;
    let scToks36: ScVector;
    let falseHead1128: ScPair;
    let scNames37: ScVector;
    let scSteps38: ScVector;
    let scPredictors39: ScVector;
    let scEnders40: ScVector;
    let scStarters41: ScVector;
    let scnts42: string[];
    let makeStates: (bgLScNbzd2toks50zd2: number, bgLScNbzd2confs51zd2: number) => ScVector;
    let bgLScConfzd2setzd2getza244za2: (state: ScVector, bgLScStatezd2num53zd2: number, scConf54: number) => ScVector;
    let confSetMergeNewBang: (confSet: ScVector) => number;
    let confSetAdjoin: (
      state: ScVector,
      confSet: ScVector,
      scConf56: number,
      i: number
    ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let bgLScConfzd2setzd2adjoinza245za2: (
      scStates57: ScVector,
      bgLScStatezd2num58zd2: number,
      l: ScPair,
      i: number
    ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let bgLScConfzd2setzd2adjoinza2za246z00: (
      scStates60: ScVector,
      bgLScStatesza261za2: ScVector,
      bgLScStatezd2num62zd2: number,
      scConf63: number,
      i: number
    ) => boolean;
    let confSetUnion: (
      state: ScVector,
      confSet: ScVector,
      scConf66: number,
      otherSet: ScVector
    ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    let forw: (
      scStates67: ScVector,
      bgLScStatezd2num68zd2: number,
      scStarters69: ScVector,
      scEnders70: ScVector,
      scPredictors71: ScVector,
      scSteps72: ScVector,
      scNts73: string[]
    ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;

    makeStates = (bgLScNbzd2toks50zd2: number, bgLScNbzd2confs51zd2: number): ScVector => {
      //debugLog('This is makeStates')
      let v: ScVector;
      let i: number;
      let scStates52: ScVector;
      scStates52 = scMakeVector(bgLScNbzd2toks50zd2 + 1, false);
      i = bgLScNbzd2toks50zd2;
      while (i >= 0) {
        v = scMakeVector(bgLScNbzd2confs51zd2 + 1, false);
        v.arr[0] = -1;
        scStates52.arr[i] = v;
        i -= 1;
      }
      return scStates52;
    };
    bgLScConfzd2setzd2getza244za2 = (state: ScVector, bgLScStatezd2num53zd2: number, scConf54: number): ScVector => {
      let confSet: ScVector;
      let bgLScConfzd2set55zd2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
      bgLScConfzd2set55zd2 = state.arr[scConf54 + 1];
      if (bgLScConfzd2set55zd2 !== false) {
        return bgLScConfzd2set55zd2 as ScVector;
      } else {
        confSet = scMakeVector(bgLScStatezd2num53zd2 + EARLEYBOYER_6, false);
        confSet.arr[1] = -3;
        confSet.arr[EARLEYBOYER_2] = -1;
        confSet.arr[EARLEYBOYER_3] = -1;
        confSet.arr[EARLEYBOYER_4] = -1;
        state.arr[scConf54 + 1] = confSet;
        return confSet;
      }
    };
    confSetMergeNewBang = (confSet: ScVector): number => {
      confSet.arr[(confSet.arr[1] as number) + EARLEYBOYER_5] = confSet.arr[EARLEYBOYER_4];
      confSet.arr[1] = confSet.arr[EARLEYBOYER_3];
      confSet.arr[EARLEYBOYER_3] = -1;
      return (confSet.arr[EARLEYBOYER_4] = -1);
    };
    confSetAdjoin = (
      state: ScVector,
      confSet: ScVector,
      scConf56: number,
      i: number
    ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
      let tail: number = confSet.arr[EARLEYBOYER_3] as number;
      confSet.arr[i + EARLEYBOYER_5] = -1;
      confSet.arr[tail + EARLEYBOYER_5] = i;
      confSet.arr[EARLEYBOYER_3] = i;
      if (tail < 0) {
        confSet.arr[0] = state.arr[0];
        state.arr[0] = scConf56;
        return scConf56;
      } else {
        return new ScNull();
      }
    };
    bgLScConfzd2setzd2adjoinza245za2 = (
      scStates57: ScVector,
      bgLScStatezd2num58zd2: number,
      l: ScPair,
      i: number
    ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
      //debugLog('This is bgLScConfzd2setzd2adjoinza245za2')
      let confSet: ScVector;
      let scConf59: number;
      let l1: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
      let state: ScVector;
      state = scStates57.arr[bgLScStatezd2num58zd2] as ScVector;
      l1 = l;
      while (l1 instanceof ScPair) {
        scConf59 = (l1 as ScPair).car as number;
        confSet = bgLScConfzd2setzd2getza244za2(state, bgLScStatezd2num58zd2, scConf59);
        if (confSet.arr[i + EARLEYBOYER_5] === false) {
          confSetAdjoin(state, confSet, scConf59, i);
          l1 = (l1 as ScPair).cdr;
        } else {
          l1 = (l1 as ScPair).cdr;
        }
      }
      return new ScNull();
    };
    bgLScConfzd2setzd2adjoinza2za246z00 = (
      scStates60: ScVector,
      bgLScStatesza261za2: ScVector,
      bgLScStatezd2num62zd2: number,
      scConf63: number,
      i: number
    ): boolean => {
      let bgLScConfzd2setza264z70: ScVector;
      let bgLScStateza265za2: ScVector;
      let confSet: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
      let state: ScVector = scStates60.arr[bgLScStatezd2num62zd2] as ScVector;
      confSet = state.arr[scConf63 + 1];
      if ((confSet !== false ? (confSet as ScVector).arr[i + EARLEYBOYER_5] : false) !== false) {
        bgLScStateza265za2 = bgLScStatesza261za2.arr[bgLScStatezd2num62zd2] as ScVector;
        bgLScConfzd2setza264z70 = bgLScConfzd2setzd2getza244za2(bgLScStateza265za2, bgLScStatezd2num62zd2, scConf63);
        (bgLScConfzd2setza264z70.arr[i + EARLEYBOYER_5] as boolean) === false ? confSetAdjoin(bgLScStateza265za2, bgLScConfzd2setza264z70, scConf63, i) : null;
        return true;
      } else {
        return false;
      }
    };
    confSetUnion = (
      state: ScVector,
      confSet: ScVector,
      scConf66: number,
      otherSet: ScVector
    ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
      //debugLog("This is confSetUnion");
      let i = otherSet.arr[EARLEYBOYER_2] as number;
      while (i >= 0) {
        if (confSet.arr[(i as number) + EARLEYBOYER_5] === false) {
          confSetAdjoin(state, confSet, scConf66, i as number);
          i = otherSet.arr[(i as number) + EARLEYBOYER_5] as number;
        } else {
          i = otherSet.arr[(i as number) + EARLEYBOYER_5] as number;
        }
      }
      return new ScNull();
    };
    forw = (
      scStates67: ScVector,
      bgLScStatezd2num68zd2: number,
      scStarters69: ScVector,
      scEnders70: ScVector,
      scPredictors71: ScVector,
      scSteps72: ScVector,
      scNts73: string[]
    ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
      //debugLog('This is forw')
      let nextSet: ScVector;
      let next: number;
      let confSet: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
      let ender: number;
      let l: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
      let starterSet: ScVector;
      let starter: number;
      let scl74: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
      let scLoop175: (
        l: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
      ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
      let head: number;
      let bgLScConfzd2set76zd2: ScVector;
      let bgLScStatezd2num77zd2: number;
      let state: ScVector;
      let scStates78: ScVector;
      let preds: ScPair;
      let bgLScConfzd2set79zd2: ScVector;
      let step: number;
      let scConf80: number;
      let bgLScNbzd2nts81zd2: number;
      let scState82: ScVector;
      scState82 = scStates67.arr[bgLScStatezd2num68zd2] as ScVector;
      bgLScNbzd2nts81zd2 = scNts73.length;
      while (true) {
        scConf80 = scState82.arr[0] as number;
        if (scConf80 >= 0) {
          step = scSteps72.arr[scConf80] as number;
          bgLScConfzd2set79zd2 = scState82.arr[scConf80 + 1] as ScVector;
          head = bgLScConfzd2set79zd2.arr[EARLEYBOYER_4] as number;
          scState82.arr[0] = bgLScConfzd2set79zd2.arr[0];
          confSetMergeNewBang(bgLScConfzd2set79zd2);
          if (step >= 0) {
            scl74 = scStarters69.arr[step];
            while (scl74 instanceof ScPair) {
              starter = (scl74 as ScPair).car as number;
              starterSet = bgLScConfzd2setzd2getza244za2(scState82, bgLScStatezd2num68zd2, starter);
              if (starterSet.arr[bgLScStatezd2num68zd2 + EARLEYBOYER_5] === false) {
                confSetAdjoin(scState82, starterSet, starter, bgLScStatezd2num68zd2);
                scl74 = (scl74 as ScPair).cdr as ScPair;
              } else {
                scl74 = (scl74 as ScPair).cdr as ScPair;
              }
            }
            l = scEnders70.arr[step];
            while (l instanceof ScPair) {
              ender = (l as ScPair).car as number;
              confSet = scState82.arr[ender + 1];
              if ((confSet !== false ? (confSet as ScVector).arr[bgLScStatezd2num68zd2 + EARLEYBOYER_5] : false) !== false) {
                next = scConf80 + 1;
                nextSet = bgLScConfzd2setzd2getza244za2(scState82, bgLScStatezd2num68zd2, next);
                confSetUnion(scState82, nextSet, next, bgLScConfzd2set79zd2);
                l = (l as ScPair).cdr;
              } else {
                l = (l as ScPair).cdr;
              }
            }
          } else {
            preds = scPredictors71.arr[step + bgLScNbzd2nts81zd2] as ScPair;
            scStates78 = scStates67;
            state = scState82;
            bgLScStatezd2num77zd2 = bgLScStatezd2num68zd2;
            bgLScConfzd2set76zd2 = bgLScConfzd2set79zd2;
            scLoop175 = (
              l: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
            ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
              let scState83: ScVector;
              let bgLScNextzd2set84zd2: ScVector;
              let scNext85: number;
              let predSet: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
              let i: number;
              let pred: number;
              if (l instanceof ScPair) {
                pred = (l as ScPair).car as number;
                i = head;
                while (i >= 0) {
                  scState83 = scStates78.arr[i] as ScVector;
                  predSet = scState83.arr[pred + 1];
                  if (predSet !== false) {
                    scNext85 = pred + 1;
                    bgLScNextzd2set84zd2 = bgLScConfzd2setzd2getza244za2(state, bgLScStatezd2num77zd2, scNext85);
                    confSetUnion(state, bgLScNextzd2set84zd2, scNext85, predSet as ScVector);
                  }
                  i = bgLScConfzd2set76zd2.arr[i + EARLEYBOYER_5] as number;
                }
                return scLoop175((l as ScPair).cdr as ScPair);
              } else {
                return new ScNull();
              }
            };
            scLoop175(preds);
          }
        } else {
          return new ScNull();
        }
      }
    };

    scnts42 = parserDescr[1] as string[];
    scStarters41 = parserDescr[EARLEYBOYER_2] as ScVector;
    scEnders40 = parserDescr[EARLEYBOYER_3] as ScVector;
    scPredictors39 = parserDescr[EARLEYBOYER_4] as ScVector;
    scSteps38 = parserDescr[EARLEYBOYER_5] as ScVector;
    scNames37 = parserDescr[EARLEYBOYER_6] as ScVector;
    falseHead1128 = new ScPair(new ScNull(), new ScNull());
    l1125 = lexer(input);
    tail1129 = falseHead1128;

    while (!(l1125 instanceof ScNull)) {
      tok = (l1125 as ScPair).car as ScPair;
      let l1 = tok.cdr;
      let l2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = new ScNull();
      while (l1 instanceof ScPair) {
        sci29 = scInd43((l1 as ScPair).car, scnts42);
        if (sci29 !== false) {
          l1 = (l1 as ScPair).cdr;
          l2 = new ScPair(sci29, l2);
        } else {
          l1 = (l1 as ScPair).cdr;
        }
      }
      scOptrOpnd22 = new ScPair(tok.car, scReverse(l2));
      scOptrOpnd21 = new ScPair(scOptrOpnd22, new ScNull());
      tail1129.cdr = scOptrOpnd21;
      tail1129 = tail1129.cdr as ScPair;
      l1125 = (l1125 as ScPair).cdr;
    }
    scOptrOpnd20 = falseHead1128.cdr as ScPair;
    scToks36 = scList2vector(scOptrOpnd20);
    bgLScNbzd2toks35zd2 = scToks36.arr.length;
    bgLScNbzd2confs34zd2 = scSteps38.arr.length;
    scStates33 = makeStates(bgLScNbzd2toks35zd2, bgLScNbzd2confs34zd2);
    goalStarters = scStarters41.arr[0] as ScPair;
    bgLScConfzd2setzd2adjoinza245za2(scStates33, 0, goalStarters, 0);
    forw(scStates33, 0, scStarters41, scEnders40, scPredictors39, scSteps38, scnts42);
    sci28 = 0;
    while (sci28 < bgLScNbzd2toks35zd2) {
      tokNts = (scToks36.arr[sci28] as ScPair).cdr;
      bgLScConfzd2setzd2adjoinza245za2(scStates33, sci28 + 1, tokNts as ScPair, sci28);
      forw(scStates33, sci28 + 1, scStarters41, scEnders40, scPredictors39, scSteps38, scnts42);
      sci28 += 1;
    }
    nbToks = scToks36.arr.length;
    bgLScNbzd2confs32zd2 = scSteps38.arr.length;
    bgLScNbzd2nts31zd2 = scnts42.length;
    bgLScStatesza230za2 = makeStates(nbToks, bgLScNbzd2confs32zd2);
    goalEnders = scEnders40.arr[0] as ScPair;
    l = goalEnders;
    while (l instanceof ScPair) {
      conf = (l as ScPair).car as number;
      bgLScConfzd2setzd2adjoinza2za246z00(scStates33, bgLScStatesza230za2, nbToks, conf, 0);
      l = (l as ScPair).cdr;
    }
    i = nbToks;
    while (i >= 0) {
      states = scStates33;
      bgLScStatesza227za2 = bgLScStatesza230za2;
      stateNum = i;
      scEnders26 = scEnders40;
      scSteps25 = scSteps38;
      bgLScNbzd2nts24zd2 = bgLScNbzd2nts31zd2;
      toks = scToks36;
      bgLScStateza223za2 = bgLScStatesza230za2.arr[i] as ScVector;
      loop1 = (): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
        let scLoop1127: (
          paramL: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
        ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
        let prev: number;
        let bgLScStatesza2128za2: ScVector;
        let scStates129: ScVector;
        let j: number;
        let i: number;
        let sci130: number;
        let head: number;

        let confSet: ScVector;
        let scConf131 = bgLScStateza223za2.arr[0] as number;
        if (scConf131 >= 0) {
          confSet = bgLScStateza223za2.arr[scConf131 + 1] as ScVector;
          head = confSet.arr[EARLEYBOYER_4] as number;
          bgLScStateza223za2.arr[0] = confSet.arr[0];
          confSetMergeNewBang(confSet);
          sci130 = head;
          while (sci130 >= 0) {
            i = sci130;
            j = stateNum;
            scStates129 = states;
            bgLScStatesza2128za2 = bgLScStatesza227za2;
            prev = scConf131 - 1;
            if (scConf131 >= bgLScNbzd2nts24zd2 && (scSteps25.arr[prev] as number) >= 0) {
              scLoop1127 = (
                paramL: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
              ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
                let k: number;
                let enderSet: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
                let state: ScVector;
                let ender: number;
                let l: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = paramL;
                while (true) {
                  if (l instanceof ScPair) {
                    ender = (l as ScPair).car as number;
                    state = scStates129.arr[j] as ScVector;
                    enderSet = state.arr[ender + 1] as ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
                    if (enderSet !== false) {
                      k = (enderSet as ScVector).arr[EARLEYBOYER_2] as number;
                      while (k >= 0) {
                        if (k >= i) {
                          if (bgLScConfzd2setzd2adjoinza2za246z00(scStates129, bgLScStatesza2128za2, k, prev, i) !== false) {
                            bgLScConfzd2setzd2adjoinza2za246z00(scStates129, bgLScStatesza2128za2, j, ender, k);
                          }
                        }
                        k = (enderSet as ScVector).arr[k + EARLEYBOYER_5] as number;
                      }
                      return scLoop1127((l as ScPair).cdr as ScPair);
                    } else {
                      l = (l as ScPair).cdr as ScPair as ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
                    }
                  } else {
                    return new ScNull();
                  }
                }
              };
              scLoop1127(scEnders26.arr[scSteps25.arr[prev] as number] as ScPair);
            }
            sci130 = confSet.arr[sci130 + EARLEYBOYER_5] as number;
          }
          return loop1();
        } else {
          return new ScNull();
        }
      };
      loop1();
      i -= 1;
    }
    optrOpnd = bgLScStatesza230za2;
    return [scnts42, scStarters41, scEnders40, scPredictors39, scSteps38, scNames37, scToks36, optrOpnd, 0, 0, 0];
  };
}

function ind(nt: string, scNts10: string[]): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  //debugLog('This is ind')
  let i = scNts10.length - 1;
  while (true) {
    if (i >= 0) {
      if (scIsEqual(scNts10[i], nt)) {
        return i;
      } else {
        i -= 1;
      }
    } else {
      return false;
    }
  }
}

function test(k: number): number {
  //debugLog('This is test')
  let constEarley = new ScPair(
    new ScPair(
      '\u1E9Cs',
      new ScPair(new ScPair('\u1E9Ca', new ScNull()), new ScPair(new ScPair('\u1E9Cs', new ScPair('\u1E9Cs', new ScNull())), new ScNull()))
    ),
    new ScNull()
  );
  let x: Array<ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector[]>;
  let p: (input: ScPair) => Array<ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector[]>;
  p = bgLMakezd2Parserzd2(constEarley, lexer);
  x = p(scVector2list(scMakeVector(k, '\u1E9Ca')) as ScPair);
  let result = scLength(bgLParsezd2ze3treesz31(x, '\u1E9Cs', 0, k) as ScPair);
  return result;
}

function lexer(l: ScPair): ScPair {
  let scX134: string;
  let falseHead1133: ScPair = new ScPair(new ScNull(), new ScNull());
  let tail1134 = falseHead1133;
  let l1130: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = l;
  while (!(l1130 instanceof ScNull)) {
    scX134 = (l1130 as ScPair).car as string;
    tail1134.cdr = new ScPair(scList(scX134, scX134), new ScNull());
    tail1134 = tail1134.cdr as ScPair;
    l1130 = (l1130 as ScPair).cdr;
  }
  return falseHead1133.cdr as ScPair;
}

function derivTrees(
  scConf91: number,
  i: number,
  j: number,
  scEnders92: ScVector,
  scSteps93: ScVector,
  scNames94: ScVector,
  scToks95: ScPair[],
  scStates96: ScVector,
  bgLScNbzd2nts97zd2: number
): ScPair {
  //debugLog("This is derivTrees")
  let scLoop198: (
    l1Param: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
    l2Param: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
  ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let prev: number;
  let name: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = scNames94.arr[scConf91];
  if (name !== false) {
    if (scConf91 < bgLScNbzd2nts97zd2) {
      return scList(scList(name as ScPair, scToks95[i].car as ScPair | string | number)) as ScPair;
    } else {
      return scList(scList(name as ScPair)) as ScPair;
    }
  } else {
    prev = scConf91 - 1;
    scLoop198 = (
      l1Param: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
      l2Param: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
    ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
      //debugLog("This is scLoop198")
      let loop2: (
        paramK: number,
        paramL2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
      ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
      let enderSet: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
      let state: ScVector;
      let ender: number;
      let l1 = l1Param;
      let l2 = l2Param;
      while (true) {
        if (l1 instanceof ScPair) {
          ender = (l1 as ScPair).car as number;
          state = scStates96.arr[j] as ScVector;
          enderSet = state.arr[ender + 1];
          if (enderSet !== false) {
            loop2 = (
              paramK: number,
              paramL2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
            ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
              let loop3: (
                l3: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
                l2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
              ) => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
              let enderTrees: ScPair;
              let prevTrees: ScPair;
              let confSet: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
              let scState99: ScVector;
              let k: number = paramK;
              let l2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = paramL2;
              while (true) {
                if (k >= 0) {
                  scState99 = scStates96.arr[k] as ScVector;
                  confSet = scState99.arr[prev + 1];
                  if (k >= i && (confSet !== false ? (confSet as ScVector).arr[i + EARLEYBOYER_5] : false) !== false) {
                    prevTrees = derivTrees(prev, i, k, scEnders92, scSteps93, scNames94, scToks95, scStates96, bgLScNbzd2nts97zd2);
                    enderTrees = derivTrees(ender, k, j, scEnders92, scSteps93, scNames94, scToks95, scStates96, bgLScNbzd2nts97zd2);
                    loop3 = (
                      l3: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
                      l2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
                    ): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector => {
                      let l4: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
                      let scL2100: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
                      let enderTree: ScPair;
                      if (l3 instanceof ScPair) {
                        enderTree = scList((l3 as ScPair).car as ScPair | string | number) as ScPair;
                        l4 = prevTrees;
                        scL2100 = l2;
                        while (l4 instanceof ScPair) {
                          scL2100 = new ScPair(scAppend((l4 as ScPair).car as ScPair, enderTree), scL2100);
                          l4 = (l4 as ScPair).cdr as ScPair;
                        }
                        return loop3((l3 as ScPair).cdr, scL2100);
                      } else {
                        return loop2((enderSet as ScVector).arr[k + EARLEYBOYER_5] as number, l2) as
                          | ScPair
                          | string
                          | number
                          | string[]
                          | boolean
                          | ScNull
                          | ScPair[]
                          | ScErrorOutputPort
                          | ScVector;
                      }
                    };
                    return loop3(enderTrees, l2);
                  } else {
                    k = (enderSet as ScVector).arr[k + EARLEYBOYER_5] as number;
                  }
                } else {
                  return scLoop198((l1 as ScPair).cdr, l2);
                }
              }
            };
            return loop2((enderSet as ScVector).arr[EARLEYBOYER_2] as number, l2);
          } else {
            l1 = (l1 as ScPair).cdr;
          }
        } else {
          return l2;
        }
      }
    };
    return scLoop198(scEnders92.arr[scSteps93.arr[prev] as number], new ScNull()) as ScPair;
  }
}

function bgLScDerivzd2treesza247z70(
  nt: string,
  i: number,
  j: number,
  scNts101: string[],
  scEnders102: ScVector,
  scSteps103: ScVector,
  scNames104: ScVector,
  scToks105: ScPair[],
  scStates106: ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  //debugLog('This is bgLScDerivzd2treesza247z70')
  let confSet: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let state: ScVector;
  let scConf107: number;
  let l: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let trees: ScPair | ScNull;
  let bgLScNbzd2nts108zd2: number;
  let bgLScNtza2109za2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  bgLScNtza2109za2 = scInd43(nt, scNts101);
  if (bgLScNtza2109za2 !== false) {
    bgLScNbzd2nts108zd2 = scNts101.length;
    l = scEnders102.arr[bgLScNtza2109za2 as number];
    trees = new ScNull();
    while (l instanceof ScPair) {
      scConf107 = (l as ScPair).car as number;
      state = scStates106.arr[j] as ScVector;
      confSet = state.arr[scConf107 + 1];

      if ((confSet !== false ? (confSet as ScVector).arr[i + EARLEYBOYER_5] : false) !== false) {
        l = (l as ScPair).cdr as ScPair;
        trees = scAppend(derivTrees(scConf107, i, j, scEnders102, scSteps103, scNames104, scToks105, scStates106, bgLScNbzd2nts108zd2), trees) as ScPair;
      } else {
        l = (l as ScPair).cdr as ScPair;
      }
    }
    return trees;
  } else {
    return false;
  }
}

function bgLParsezd2ze3treesz31(
  parse: Array<ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector[]>,
  nt: string,
  i: number,
  j: number
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  //debugLog('This is bgLParsezd2ze3treesz31')
  let states: ScVector;
  let toks: ScPair[];
  let names: ScVector;
  let steps: ScVector;
  let enders: ScVector;
  let nts: string[];
  nts = parse[0] as string[];
  enders = parse[EARLEYBOYER_2] as ScVector;
  steps = parse[EARLEYBOYER_4] as ScVector;
  names = parse[EARLEYBOYER_5] as ScVector;
  toks = (parse[EARLEYBOYER_6] as ScVector).arr as ScPair[];
  states = parse[EARLEYBOYER_7] as ScVector;
  return bgLScDerivzd2treesza247z70(nt, i, j, nts, enders, steps, names, toks, states);
}

function scInd43(nt: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector, scNts49: string[]): number | boolean {
  //debugLog('This is scInd43')
  let i: number = scNts49.length - 1;
  while (true) {
    if (i >= 0) {
      if (scIsEqual(scNts49[i], nt)) {
        return i;
      } else {
        i -= 1;
      }
    } else {
      return false;
    }
  }
}

function scNumber2jsstring(x: number): string {
  //debugLog('This is scNumber2jsstring')
  return x.toString();
}

class ScErrorOutputPort {
  appendJSString(s: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector): void {
    return;
  }
  close(): void {}
}

let scDefaultOut = new ScErrorOutputPort();

function scToWriteString(o: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector): string {
  //debugLog('This is scToWriteString')

  if (o instanceof ScNull) {
    return '()';
  } else if (typeof o === 'boolean') {
    if (o === true) {
      return '#t';
    }
    return '#f';
  } else if (typeof o === 'number') {
    return o.toString();
  }
  return '';
}

function scToDisplayString(o: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector): string {
  //debugLog('This is scToDisplayString')

  if (o instanceof ScNull) {
    return '()';
  } else if (typeof o === 'boolean') {
    if (o === true) {
      return '#t';
    }
    return '#f';
  } else if (typeof o === 'number') {
    return o.toString();
  }
  return '';
}

function scList(...args: (ScPair | string | number)[]): ScPair {
  let res: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = new ScNull();
  let i = args.length - 1;
  while (i >= 0) {
    res = new ScPair(args[i], res);
    i -= 1;
  }
  return res as ScPair;
}

function scWrite(
  o: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  p?: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): void {
  //debugLog('This is scWrite')

  let p1 = p;
  if (p1 instanceof ScNull) {
    p1 = scDefaultOut;
  }
  (p1 as ScErrorOutputPort).appendJSString(scToWriteString(o));
}

function scDisplay(
  o: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  p: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): void {
  //debugLog('This is scDisplay')

  let p1 = p;
  if (p1 instanceof ScNull) {
    p1 = scDefaultOut;
  }
  (p1 as ScErrorOutputPort).appendJSString(scToDisplayString(o));
}

function scIsNumber(n: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector): boolean {
  return typeof n === 'number';
}

function scCons(
  car: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  cdr: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair {
  return new ScPair(car, cdr);
}

function scIsPair(p: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector): boolean {
  return p instanceof ScPair;
}

function scIsPairEqual(
  p1: ScPair,
  p2: ScPair,
  comp: (
    a: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
    b: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
  ) => boolean
): boolean {
  return comp(p1.car, p2.car) && comp(p1.cdr, p2.cdr);
}

function scIsVectorEqual(
  v1: ScVector,
  v2: ScVector,
  comp: (
    a: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
    b: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
  ) => boolean
): boolean {
  if (v1.arr.length !== v2.arr.length) {
    return false;
  }
  for (let i = 0; i < v1.arr.length; i++) {
    if (!comp(v1.arr[i], v2.arr[i])) {
      return false;
    }
  }
  return true;
}

function scIsVector(v: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector): boolean {
  return v instanceof ScVector;
}

function scIsEqual(
  o1: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  o2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): boolean {
  let resultAny: boolean;
  let resultPair: boolean;
  let resultVector: boolean;
  let resultString: boolean;

  resultString = typeof o1 === 'string' && typeof o2 === 'string' && (o1 as string) === (o2 as string);
  resultAny = o1 === o2;
  resultPair = scIsPair(o1) && scIsPair(o2) && scIsPairEqual(o1 as ScPair, o2 as ScPair, scIsEqual);
  resultVector = scIsVector(o1) && scIsVector(o2) && scIsVectorEqual(o1 as ScVector, o2 as ScVector, scIsEqual);
  if (resultAny || resultPair || resultVector || resultString) {
    return true;
  } else {
    return false;
  }
}

function scAssq(
  o: string,
  al: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  let aln: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = al;

  while (!(aln instanceof ScNull)) {
    if ((((aln as ScPair).car as ScPair).car as string) === o) {
      return (aln as ScPair).car;
    }
    aln = (aln as ScPair).cdr;
  }
  return false;
}

function bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer(
  sym: string
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  //debugLog('This is bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer');
  let r: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let x: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  x = scAssq(sym, bgLScZa2symbolzd2recordszd2alistza22z00Nboyer);
  if (x instanceof ScPair) {
    return (x as ScPair).cdr;
  } else {
    r = new ScVector([sym, new ScNull()]);
    bgLScZa2symbolzd2recordszd2alistza22z00Nboyer = new ScPair(new ScPair(sym, r), bgLScZa2symbolzd2recordszd2alistza22z00Nboyer);
    return r;
  }
}

function translateTermNboyer(
  term: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  //debugLog('This is translateTermNboyer');
  let lst: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let cdrValue: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  if (!(term instanceof ScPair)) {
    return term;
  }
  lst = (term as ScPair).cdr;
  if (lst instanceof ScNull) {
    cdrValue = new ScNull();
    return new ScPair(bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer((term as ScPair).car as string), cdrValue);
  } else {
    return new ScPair(
      bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer((term as ScPair).car as string),
      new ScPair(translateTermNboyer((lst as ScPair).car), translateArgsNboyer((lst as ScPair).cdr))
    );
  }
}

function translateArgsNboyer(
  lst: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  //debugLog('This is translateArgsNboyer');
  let scLst5: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let term: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let carValue: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let cdrValue: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  if (lst instanceof ScNull) {
    return new ScNull();
  }
  term = (lst as ScPair).car;
  carValue = !(term instanceof ScPair) ?
    term :
    new ScPair(bgLScSymbolzd2ze3Symbolzd2Record1ze3Nboyer((term as ScPair).car as string), translateArgsNboyer((term as ScPair).cdr));
  scLst5 = (lst as ScPair).cdr;
  if (scLst5 instanceof ScNull) {
    cdrValue = new ScNull();
  } else {
    cdrValue = new ScPair(translateTermNboyer((scLst5 as ScPair).car), translateArgsNboyer((scLst5 as ScPair).cdr));
  }
  return new ScPair(carValue, cdrValue);
}

function applySubstNboyer(
  alist: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  term: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  let lst: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let tempTemp: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  if (!(term instanceof ScPair)) {
    tempTemp = scAssq(term as string, alist);
    if (tempTemp instanceof ScPair) {
      return (tempTemp as ScPair).cdr;
    } else {
      return term;
    }
  } else {
    lst = (term as ScPair).cdr;
    if (lst instanceof ScNull) {
      return new ScPair((term as ScPair).car, new ScNull());
    } else {
      return new ScPair((term as ScPair).car, new ScPair(applySubstNboyer(alist, (lst as ScPair).car), applySubstLstNboyer(alist, (lst as ScPair).cdr)));
    }
  }
}

function isTermMemberNboyer(
  x: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  lst: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): boolean {
  //debugLog('This is isTermMemberNboyer');
  let tempLst = lst;
  while (true) {
    if (tempLst instanceof ScNull) {
      return false;
    } else if (isTermEqualNboyer(x, (tempLst as ScPair).car) !== false) {
      return true;
    } else {
      tempLst = (tempLst as ScPair).cdr;
    }
  }
}

function isTermEqualNboyer(
  x: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  y: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): boolean {
  let lst1: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let lst2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let r2: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  let r1: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  if (x instanceof ScPair) {
    if (y instanceof ScPair) {
      r1 = (x as ScPair).car;
      r2 = (y as ScPair).car;
      if ((r1 === r2) !== false) {
        lst1 = (x as ScPair).cdr;
        lst2 = (y as ScPair).cdr;
        while (true) {
          if (lst1 instanceof ScNull) {
            return lst2 instanceof ScNull;
          } else if (lst2 instanceof ScNull) {
            return false;
          } else if (isTermEqualNboyer((lst1 as ScPair).car, (lst2 as ScPair).car) !== false) {
            lst1 = (lst1 as ScPair).cdr;
            lst2 = (lst2 as ScPair).cdr;
          } else {
            return false;
          }
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  } else {
    return scIsEqual(x, y);
  }
}

function scMember(
  o: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  l: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  let current: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = l;

  while (current instanceof ScPair) {
    if (scIsEqual((current as ScPair).car as ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector, o)) {
      return current;
    }
    current = (current as ScPair).cdr;
  }
  return false;
}

function scMakeVector(size: number, fill: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector): ScVector {
  let nullArr: Array<ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector> = [];
  for (let i = 0; i < size; i++) {
    nullArr.push(new ScNull());
  }
  let a: ScVector = new ScVector(nullArr);
  if (!(fill instanceof ScNull)) {
    let i = 0;
    while (i < a.arr.length) {
      a.arr[i] = fill;
      i += 1;
    }
  }
  return a;
}

function scVector2list(a: ScVector): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  let res: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = new ScNull();
  let i = a.arr.length - 1;
  while (i >= 0) {
    res = scCons(a.arr[i], res);
    i -= 1;
  }
  return res;
}

function scList2vector(l: ScPair): ScVector {
  let a: ScVector = new ScVector([]);
  let list: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = l;
  while (!(list instanceof ScNull)) {
    a.arr.push((list as ScPair).car);
    list = (list as ScPair).cdr;
  }
  return a;
}

function scLength(l: ScPair): number {
  let res = 0;
  let l1: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = l;
  while (!(l1 instanceof ScNull)) {
    res += 1;
    l1 = (l1 as ScPair).cdr;
  }
  return res;
}

function scReverseAppendBang(l1: ScPair | null, l2: ScPair | null): ScPair | null {
  let res = l2;
  let current = l1;
  while (!(current instanceof ScNull)) {
    let tmp = res;
    res = current;
    current = (current as ScPair).cdr as ScPair;
    (res as ScPair).cdr = tmp as Object;
  }
  return res;
}

function scDualAppend(l1: ScPair | null, l2: ScPair | null): ScPair | null {
  if (l1 === null) {
    return l2;
  }
  if (l2 === null) {
    return l1;
  }
  let rev: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = scReverse(l1);
  return scReverseAppendBang(rev as ScPair, l2);
}

function scAppend(...args: (ScPair | ScNull)[]): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  if (args.length === 0) {
    return new ScNull();
  }
  let res: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = args[args.length - 1];
  let i = args.length - EARLEYBOYER_2;
  while (i >= 0) {
    res = scDualAppend(args[i] as ScPair, res as ScPair) as ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
    i -= 1;
  }
  return res;
}

function scReverse(
  l1: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector
): ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector {
  let res: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = new ScNull();
  let current: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = l1;
  while (!(current instanceof ScNull)) {
    res = scCons((current as ScPair).car, res);
    current = (current as ScPair).cdr as ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector;
  }
  return res;
}

function bgEarleyzd2Benchmarkzd2(...argumentsAry: string[]): void {
  //debugLog('This is bgEarleyzd2Benchmarkzd2')
  let args: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = new ScNull();
  let sctmp = argumentsAry.length - 1;
  while (sctmp >= 0) {
    args = scCons(argumentsAry[sctmp], args);
    sctmp -= 1;
  }
  let k: number = args instanceof ScNull ? EARLEYBOYER_7 : ((args as ScPair).car as number);
  let warn: (result: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector) => boolean = result => {
    scDisplay(result, new ScNull());
    scNewline(new ScNull());
    return result === EARLEYBOYER_132;
  };
  let run: () => number = () => {
    return test(k);
  };
  return bgLRunzd2Benchmarkzd2('earley', 1, run, warn);
}

function scNewline(p?: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector): void {
  let p1 = p;
  if (p1 instanceof ScNull) {
    // we assume not given
    p1 = scDefaultOut;
  }
  (p1 as ScErrorOutputPort).appendJSString('\n');
}

function runBenchmark(
  name: string,
  count: number,
  run: () => ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector,
  warn: (param: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector) => boolean
): void {
  //debugLog('This is runBenchmark')
  for (let n = 0; n < count; ++n) {
    let result: ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector = run();
    //debugLog("result: " + result)
    if (!warn(result)) {
      return;
    }
  }
}

let bgLRunzd2Benchmarkzd2 = runBenchmark;
/*
 * @State
 * @Tags Jetstream2
 */
class Benchmark {
  /*
   *@Benchmark
   */
  runIteration(): void {
    bgEarleyzd2Benchmarkzd2();
    bgNboyerzd2Benchmarkzd2();
  }
}

class ScVector {
  arr: Array<ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector>;
  constructor(arr: Array<ScPair | string | number | string[] | boolean | ScNull | ScPair[] | ScErrorOutputPort | ScVector>) {
    this.arr = arr;
  }
}

function earleyBoyerRunIteration(): void {
  let start = ArkTools.timeInUs();
  for (let i = 0; i < EARLEYBOYER_120; ++i) {
    new Benchmark().runIteration();
  }
  let end = ArkTools.timeInUs();
  print('earley-boyer: ms = ' + String((end - start) / EARLEYBOYER_1000));
}

declare interface ArkTools {
  timeInUs(args: number): number;
}

function debugLog(str: string): void {
  const isLog = false;
  if (isLog) {
    print(str);
  }
}

earleyBoyerRunIteration();
