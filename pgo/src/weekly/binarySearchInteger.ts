// declare function print(arg:string) : string;

declare interface ArkTools{
  timeInUs(arg:any):number
}

function ComplexIntegerNumeric() {
  let count: number = 5000000;
  let testArray: Int32Array = new Int32Array([1, 2, 5, 7, 9, 12, 14, 23, 32, 43, 67, 77, 89, 90, 101, 122, 125, 142, 153]); // GenerateSearchArrayInteger();
  let start: number = ArkTools.timeInUs();
  let res: number = testArray[2];
  let testArrayLength: number = testArray.length;
  for (let i: number = 1; i < count; i++) {
    let value: number = testArray[i % res & (testArrayLength - 1)];
    let low: number = 0;
    let tmp: number = 0;
    let high: number = testArrayLength - 1; // 1023
    let middle: number = high >>> 1;
    for (; low <= high; middle = (low + high) >>> 1) {
      const test: number = testArray[middle];
      if (test > value) {
        high = middle - 1;
      } else if (test < value) {
        low = middle + 1;
      } else {
        tmp = middle;
        break;
      }
    }
    res += tmp;
  }
  let end: number = ArkTools.timeInUs();
  print(""+res);
  let time = (end - start) / 1000
  print("Numerical Calculation - ComplexIntegerNumeric:\t"+String(time)+"\tms");
  return time;
}

ComplexIntegerNumeric()

