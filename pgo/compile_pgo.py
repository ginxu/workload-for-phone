import inspect
import os
import shutil
import subprocess
import sys
import datetime

SUFFIX = ['ts', 'js']
SRC_PART = 'src'  # 源文件归纳路径
OUT_PART = 'out'  # 目标文件归纳路径
TOOLS_PART = 'tools'  # 编译出来的东西拷一份进来用
HOS_PART = 'hos'  # 给HOS用
LINUX_PART = 'linux'  # 在LINUX上用
OHOS_PART = 'ohos'  # 给OHOS用
WORK_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # 当前脚本工作目录
ARK_COMPILER_PATH = os.path.join(WORK_DIR, '/home/gin/work/ark-standalone')  # 最近编译器路径

ES2ABC = os.path.join(WORK_DIR, TOOLS_PART, LINUX_PART, 'es2abc')  # 使用这三个命令编译
ARK_JS_VM = os.path.join(WORK_DIR, TOOLS_PART, LINUX_PART, 'ark_js_vm')
ARK_AOT_COMPILER = os.path.join(WORK_DIR, TOOLS_PART, LINUX_PART, 'ark_aot_compiler')

# ----编译出来的工具集----
# linux上用这些
COMPILED_X64_ES2ABC = os.path.join(ARK_COMPILER_PATH, 'out/x64.release/arkcompiler/ets_frontend/es2abc')
COMPILED_X64_ARK_COMPILER = os.path.join(ARK_COMPILER_PATH, 'out/x64.release/arkcompiler/ets_runtime/ark_aot_compiler')
COMPILED_X64_JS_VM = os.path.join(ARK_COMPILER_PATH, 'out/x64.release/arkcompiler/ets_runtime/ark_js_vm')
COMPILED_X64_LIBARK_JSRUNTIME_SO = os.path.join(ARK_COMPILER_PATH,
                                                'out/x64.release/arkcompiler/ets_runtime/libark_jsruntime.so')
COMPILED_X64_LIBSEC_SHARED_SO = os.path.join(ARK_COMPILER_PATH,
                                             'out/x64.release/thirdparty/bounds_checking_function/libsec_shared.so')
COMPILED_X64_LIBHMICUI18N_SO = os.path.join(ARK_COMPILER_PATH,
                                            'out/x64.release/thirdparty/icu/libhmicui18n.so')
COMPILED_X64_LIBHMICUUC_SO = os.path.join(ARK_COMPILER_PATH,
                                          'out/x64.release/thirdparty/icu/libhmicuuc.so')
COMPILED_X64_ICUDT69L_DAT = os.path.join(ARK_COMPILER_PATH,
                                          'third_party/icu/ohos_icu4j/data/icudt69l.dat')
# 这些要push进HOS
COMPILED_ANDROID_JS_VM = os.path.join(ARK_COMPILER_PATH, 'out/android_arm64.release/arkcompiler/ets_runtime/ark_js_vm')
COMPILED_ANDROID_LIBARK_JSRUNTIME_SO = os.path.join(ARK_COMPILER_PATH,
                                                    'out/android_arm64.release/arkcompiler/ets_runtime/libark_jsruntime.so')
COMPILED_ANDROID_LIBSEC_SHARED_SO = os.path.join(ARK_COMPILER_PATH,
                                                 'out/android_arm64.release/thirdparty/bounds_checking_function/libsec_shared.so')
COMPILED_ANDROID_LIBHMICUI18N_SO = os.path.join(ARK_COMPILER_PATH,
                                                'out/android_arm64.release/clang_x64/thirdparty/icu/libhmicui18n.so')
COMPILED_ANDROID_LIBHMICUUC_SO = os.path.join(ARK_COMPILER_PATH,
                                              'out/android_arm64.release/clang_x64/thirdparty/icu/libhmicuuc.so')

# 这个要push进OHOS
COMPILED_LIB_ARK_BUILTINS_D_ABC = os.path.join(ARK_COMPILER_PATH,
                                               'out/x64.release/obj/arkcompiler/ets_runtime/lib_ark_builtins/es2abc/lib_ark_builtins.d.abc')
# ---------------------

ES2ABC_CMD = ES2ABC + " {input_src} --type-extractor --module --merge-abc --extension={suf} --output {out_path}.abc"
ES2ABC_FILES_CMD = ES2ABC + " @{case_name}/fileInfo.txt --type-extractor --module --merge-abc  --output {out_path}.abc"

#解释器+第一次采集
ARK_JS_VM_CMD1 = ARK_JS_VM + " --enable-pgo-profiler=true --compiler-pgo-profiler-path={case_name}.ap --icu-data-path="+os.path.join(WORK_DIR, TOOLS_PART, HOS_PART) + " --entry-point={case_name} {case_name}.abc"
#PGO+第二次采集
ARK_JS_VM_CMD2 = ARK_JS_VM + " --compiler-enable-lowering-builtin=true --enable-pgo-profiler=true --aot-file={case_name} --compiler-pgo-profiler-path={case_name}.ap --icu-data-path="+os.path.join(WORK_DIR, TOOLS_PART, HOS_PART) + " --entry-point={case_name} {case_name}.abc"
#第一次AOT
ARK_AOT_COMPILER_CMD1 = ARK_AOT_COMPILER + " --compiler-enable-lowering-builtin=true --compiler-opt-loop-peeling=true --compiler-opt-inlining=true --builtins-dts=/home/gin/work/ark-standalone/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --enable-pgo-profiler=true --compiler-pgo-profiler-path={case_name}.ap --aot-file={case_name} {case_name}.abc"
#第二次AOT
ARK_AOT_COMPILER_CMD2 = ARK_AOT_COMPILER + " --compiler-enable-lowering-builtin=true --compiler-opt-loop-peeling=true --compiler-opt-inlining=true --builtins-dts=/home/gin/work/ark-standalone/arkcompiler/ets_runtime/ecmascript/ts_types/lib_ark_builtins.d.abc --enable-pgo-profiler=true --compiler-pgo-profiler-path={case_name}.ap  --aot-file={case_name} --compiler-target-triple=aarch64-unknown-linux-gnu {case_name}.abc"

"""
引入localCmp：https://gitee.com/panzhenyu1/ark_js_runtime/wikis/ark_js_vm%E6%89%A7%E8%A1%8Cabc%E6%B7%BB%E5%8A%A0icu%E6%95%B0%E6%8D%AE%E5%BA%93%E8%B7%AF%E5%BE%84
https://gitee.com/openharmony/arkcompiler_ets_runtime/issues/I6BAGT
"""
def exe_cmd(cmd):
    """执行构造好的编译命令"""
    print(cmd)
    subprocess.call(cmd, shell=False)


def copy_tools():
    """把编译出来的最新文件拷贝到本项目"""
    exe_cmd(['cp', COMPILED_X64_ES2ABC, ES2ABC])
    exe_cmd(['cp', COMPILED_X64_JS_VM, ARK_JS_VM])
    exe_cmd(['cp', COMPILED_X64_ARK_COMPILER, ARK_AOT_COMPILER])
    exe_cmd(['cp', COMPILED_X64_LIBARK_JSRUNTIME_SO, os.path.join(WORK_DIR, TOOLS_PART, LINUX_PART)])
    exe_cmd(['cp', COMPILED_X64_LIBSEC_SHARED_SO, os.path.join(WORK_DIR, TOOLS_PART, LINUX_PART)])
    exe_cmd(['cp', COMPILED_X64_LIBHMICUI18N_SO, os.path.join(WORK_DIR, TOOLS_PART, LINUX_PART)])
    exe_cmd(['cp', COMPILED_X64_LIBHMICUUC_SO, os.path.join(WORK_DIR, TOOLS_PART, LINUX_PART)])
    # exe_cmd(['cp', COMPILED_X64_ICUDT69L_DAT, os.path.join(WORK_DIR, TOOLS_PART, LINUX_PART)])

    exe_cmd(['cp', COMPILED_ANDROID_JS_VM, os.path.join(WORK_DIR, TOOLS_PART, HOS_PART)])
    exe_cmd(['cp', COMPILED_ANDROID_LIBARK_JSRUNTIME_SO, os.path.join(WORK_DIR, TOOLS_PART, HOS_PART)])
    exe_cmd(['cp', COMPILED_ANDROID_LIBSEC_SHARED_SO, os.path.join(WORK_DIR, TOOLS_PART, HOS_PART)])
    exe_cmd(['cp', COMPILED_ANDROID_LIBHMICUI18N_SO, os.path.join(WORK_DIR, TOOLS_PART, HOS_PART)])
    exe_cmd(['cp', COMPILED_ANDROID_LIBHMICUUC_SO, os.path.join(WORK_DIR, TOOLS_PART, HOS_PART)])
    exe_cmd(['cp', COMPILED_X64_ICUDT69L_DAT, os.path.join(WORK_DIR, TOOLS_PART, HOS_PART)])

    exe_cmd(['cp', COMPILED_LIB_ARK_BUILTINS_D_ABC, os.path.join(WORK_DIR, TOOLS_PART, OHOS_PART)])
    exe_cmd(['cp', COMPILED_X64_ICUDT69L_DAT, os.path.join(WORK_DIR, TOOLS_PART, OHOS_PART)])


def compile_pgo(case, src_dir, out_dir, suf='ts'):
    """构造编译命令并编译"""
    out_path = os.path.join(out_dir, case)
    param = {
        'out_path': out_path,
        'input_src': os.path.join(src_dir, f'{case}.{suf}'),
        # 'src_dir': src_dir,
        'suf': suf,
        'case_name': case,
    }
    # EXPORT_CMD = ["export", f"LD_LIBRARY_PATH={os.path.join(WORK_DIR, TOOLS_PART, LINUX_PART)}"]
    os.environ['LD_LIBRARY_PATH'] = os.path.join(WORK_DIR, TOOLS_PART, LINUX_PART)

    if suf == 'dir':
        es2abc_cmd = ES2ABC_FILES_CMD.format(**param).split()
    else:
        es2abc_cmd = ES2ABC_CMD.format(**param).split()
    os.chdir(src_dir)  # 切到源码的目录跑
    exe_cmd(es2abc_cmd)

    os.chdir(out_dir)  
    js_vm_cmd = ARK_JS_VM_CMD1.format(**param).split()
    exe_cmd(js_vm_cmd)

    aot_compiler_cmd = ARK_AOT_COMPILER_CMD1.format(**param).split()
    exe_cmd(aot_compiler_cmd)

   
    js_vm_cmd = ARK_JS_VM_CMD2.format(**param).split()
    exe_cmd(js_vm_cmd)

    aot_compiler_cmd = ARK_AOT_COMPILER_CMD2.format(**param).split()
    exe_cmd(aot_compiler_cmd)

    git_add_cmd = ['git', 'add', f"{out_path}.abc", f"{out_path}.an", f"{out_path}.ai", f"{out_path}.ap"]
    exe_cmd(git_add_cmd)
    exe_cmd(['git', 'add', out_dir])
    print(f'{case}编译结束!')


def run_one(workload):
    """
    编一个目录
    :param workload:
    :return:
    """
    out_path = os.path.join(WORK_DIR, OUT_PART, workload)
    if os.path.exists(out_path):
        shutil.rmtree(out_path)
    os.mkdir(out_path)

    src_path = os.path.join(WORK_DIR, SRC_PART, workload)
    target_src = []
    for (root, dirs, files) in os.walk(src_path):
        if root == src_path:
            for file in files:
                for suf in SUFFIX:
                    if file.endswith(f'.{suf}'):
                        target_src.append((file[:-len(suf) - 1], suf))
                        break
            for dir_ in dirs:
                target_src.append((dir_, 'dir'))

    if not target_src:
        sys.stderr.write(f'[warning]目录{workload}下不存在{SUFFIX}文件或文件夹！\n')
        return
    for case, suf in target_src:
        compile_pgo(case, src_path, out_path, suf)


def run(workloads=None):
    """
    编译指定的目录
    :param workloads:
    :return:
    """
    exe_cmd(['git', 'pull'])
    if not workloads:
        workloads = []
        fa = os.path.join(WORK_DIR, SRC_PART)
        for (root, dirs, files) in os.walk(fa):
            if fa == root:
                workloads.extend(dirs)
                # print(root, dirs, files)

    start_time = datetime.datetime.now()
    copy_tools()
    for workload in workloads:
        run_one(workload)

    if workloads:
        exe_cmd(['git', 'add', os.path.join(WORK_DIR, TOOLS_PART)])
        git_commit_cmd = ['git', 'commit', '-m', f'批量编译{workloads}']
        exe_cmd(git_commit_cmd)
        exe_cmd(['git', 'pull'])
        git_push_cmd = ['git', 'push']
        exe_cmd(git_push_cmd)
        print('git push 结束！')
    print("编译脚本共计花费时间[", (datetime.datetime.now() - start_time).seconds, ']秒')


def main(argv):
    if len(argv) > 1:
        print('编译目标集', argv[1:])
        run(argv[1:])
    else:
        print('请指定目标集！')


if __name__ == '__main__':
    main(sys.argv)
    # param = {'a':1,"b":2,'c':3}
    # s = '{a},,{b}..'
    # print(s.format(**param))
#     print(' '.join(['export', 'LD_LIBRARY_PATH=/mnt/data/l00526389/lsl/soft/ohcompiler-daily/pgo/tools/linux', '&&', '/mnt/data/l00526389/lsl/soft/ohcompiler-daily/pgo/tools/linux/es2abc', '/mnt/data/l00526389/lsl/soft/ohcompiler-daily/pgo/src/test1/helloworld.ts', '--type-extractor', '--module', '--merge-abc', '--extension=ts', '--output', '/mnt/data/l00526389/lsl/soft/ohcompiler-daily/pgo/out/test1/helloworld.abc']
# ))
